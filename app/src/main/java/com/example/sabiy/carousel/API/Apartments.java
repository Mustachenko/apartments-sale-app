package com.example.sabiy.carousel.API;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class Apartments {
    @SerializedName("apartments")
    private List<House> houses;

    public Apartments(List<House> houses) {
        this.houses = houses;
    }

    public List<House> getHouses() {
        return houses;
    }
}

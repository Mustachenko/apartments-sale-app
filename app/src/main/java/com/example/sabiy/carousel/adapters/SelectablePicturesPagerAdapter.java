package com.example.sabiy.carousel.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.dataWrapper.GallerySinglePicture;
import com.ortiz.touch.TouchImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import cn.refactor.library.SmoothCheckBox;

public class SelectablePicturesPagerAdapter extends PagerAdapter {
    private List<GallerySinglePicture> pictures;

    public SelectablePicturesPagerAdapter(List<GallerySinglePicture> pictures) {
        this.pictures = pictures;
    }

    public List<GallerySinglePicture> getPictures() {
        return pictures;
    }

    @Override
    public int getCount() {
        return pictures.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout relativeLayout = (RelativeLayout) layoutInflater.inflate(R.layout.gallery_multy_full_screen_picture_layout, container, false);

        TouchImageView imageView = relativeLayout.findViewById(R.id.picture);
        Picasso.get().setLoggingEnabled(true);
                Uri pathUri = Uri.fromFile(new File(pictures.get(position).getPicture()));
        Picasso.get().load(pathUri).resize(container.getContext().getResources().getDimensionPixelSize(R.dimen.gallery_picture_width), container.getContext().getResources().getDimensionPixelSize(R.dimen.gallery_picture_height)).centerInside().into(imageView);

        SmoothCheckBox checkBox = relativeLayout.findViewById(R.id.check_to_add);
        checkBox.bringToFront();

        checkBox.setOnCheckedChangeListener((smoothCheckBox, b) -> {
            if (!b) {
                pictures.get(position).setActive(false);
            } else {
                pictures.get(position).setActive(true);
            }
        });

        if (pictures.get(position).isActive()) {
            checkBox.setChecked(true);
        }
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(relativeLayout, 0);
        return relativeLayout;

    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }
}

package com.example.sabiy.carousel.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.sabiy.carousel.API.House;
import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.Waiting;
import com.example.sabiy.carousel.views.activities.PropositionsActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;


public class NearbyPropAdapter extends PagerAdapter {
    public static final String HOUSES = "houses";
    private List<House> houses;
    public Waiting waiting;

    public void updateHouses(List<House> newHouses) {
        houses = newHouses;
        notifyDataSetChanged();
    }

    public NearbyPropAdapter(List<House> houses) {
        this.houses = houses;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }

    @Override
    public int getCount() {
        return houses.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) layoutInflater.inflate(R.layout.location_propositions_layout, null);

        ImageView preview = view.findViewById(R.id.preview);
        TextView price = view.findViewById(R.id.prise);
        TextView label = view.findViewById(R.id.label);
        TextView reviews = view.findViewById(R.id.reviews);
        view.setLayoutParams(new ViewGroup.LayoutParams((int) container.getResources().getDimension(R.dimen.location_prop_width), ViewGroup.LayoutParams.MATCH_PARENT));
        ProgressBar progressBar = view.findViewById(R.id.image_load_progress);
        View wrapper = view.findViewById(R.id.container);
        view.setOnClickListener(v -> {

            waiting.startWaiting();
            Intent intent = new Intent(container.getContext(), PropositionsActivity.class);
            intent.putExtra(PropositionsAdapter.POSITION, position);
            intent.putParcelableArrayListExtra(HOUSES, (ArrayList<? extends Parcelable>) houses);
            container.getContext().startActivity(intent);
        });

        Picasso.get().load(houses.get(position).getPreview()).into(new Target() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                if(progressBar != null) progressBar.setVisibility(View.GONE);
                preview.setImageBitmap(bitmap);
                Palette.from(bitmap)
                        .generate(palette -> {
                            GradientDrawable imageForegroundGradient = new GradientDrawable();
                            Palette.Swatch vSwatch = palette.getLightVibrantSwatch();
                            if (vSwatch == null) vSwatch = palette.getLightMutedSwatch();
                            if (vSwatch == null) vSwatch = palette.getDominantSwatch();
                            Palette.Swatch lvSwatch = palette.getLightVibrantSwatch();
                            if (vSwatch != null && lvSwatch != null) {
                                imageForegroundGradient.setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
                                wrapper.setBackground(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{vSwatch.getRgb(), lvSwatch.getRgb()}));
                                imageForegroundGradient.setColors(new int[] {wrapper.getResources().getColor(android.R.color.transparent), wrapper.getResources().getColor(android.R.color.transparent), vSwatch.getRgb()});
                                preview.setForeground(imageForegroundGradient);
                                price.setTextColor(vSwatch.getTitleTextColor());
                                label.setTextColor(vSwatch.getBodyTextColor());
                                reviews.setTextColor(vSwatch.getBodyTextColor());
                            }
                        });
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {}

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (progressBar != null) progressBar.setVisibility(View.VISIBLE);
            }
        });

        label.setText(houses.get(position).getLabel());
        price.setText(houses.get(position).getPrice() + " $");
        reviews.setText(houses.get(position).getReviews());
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);
        return view;
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }
}

package com.example.sabiy.carousel.views.fragments;


import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.sabiy.carousel.API.House;
import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.Waiting;
import com.example.sabiy.carousel.adapters.NearbyPropAdapter;
import com.example.sabiy.carousel.adapters.PropositionsAdapter;
import com.example.sabiy.carousel.dataWrapper.Filtres;
import com.example.sabiy.carousel.viewModels.HomeFragmentViewModel;
import com.example.sabiy.carousel.views.activities.HomeActivity;
import com.example.sabiy.carousel.views.activities.LoginActivity;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import nl.dionsegijn.steppertouch.StepperTouch;

import static android.support.constraint.Constraints.TAG;


public class HomeFragment extends BaseFragment implements NavigationView.OnNavigationItemSelectedListener {

    private GridLayoutManager manager;
    private NestedScrollView filterView;
    private LinearLayout locationRequestLayout;
    private StepperTouch roomsCount;
    private ActionBarDrawerToggle toggle;
    private SwipeRefreshLayout swipeRefreshLayout;
    private DrawerLayout root;
    private RecyclerView recyclerView;
    private Fragment fragment;
    private String fragmentTag;
    private NavigationView draverList;
    private int maxValue, minValue;
    private LinearLayout areaLayout;
    private ProgressBar progressBar;
    private HomeFragmentViewModel viewModel;
    private ViewPager pager;
    private TextView locationMessage;
    private LinearLayout locationLayout;
    private  AnimationDrawable locationAnimation;
    private RecyclerView.OnItemTouchListener onItemClickListener = new RecyclerView.OnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {
            return true;
        }

        @Override
        public void onTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean b) {

        }
    };
    private Waiting waiting;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        waiting = () -> {
            if (getContext() != null) {
                progressBar = new ProgressBar(getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.CENTER;
                int progressPadding = getResources().getDimensionPixelOffset(R.dimen.loadApartmentProgressPadding);
                recyclerView.addOnItemTouchListener(onItemClickListener);
                pager.setVisibility(View.INVISIBLE);
                progressBar.setPadding(progressPadding, progressPadding, progressPadding, progressPadding);
                progressBar.setBackgroundResource(R.color.progress_background);
                progressBar.setAlpha(1);
                root.addView(progressBar, 1);
            }
        };
    }


    private AdapterView.OnItemSelectedListener orderByItemClickListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Comparator<House> comparator = (house, t1) -> {
                switch (i) {
                    case 0:
                        if (house.favourite()) {
                            return 1;
                        } else if (t1.favourite()) {
                            return -1;
                        }
                    case 1:
                        if (house.getPrice() > t1.getPrice()) {
                            return 1;
                        } else if (house.getPrice() < t1.getPrice() || house.getPrice().equals(t1.getPrice())) {
                            return -1;
                        }
                    case 2:
                        if (house.getPrice() > t1.getPrice()) {
                            return -1;
                        } else if (house.getPrice() < t1.getPrice() || house.getPrice().equals(t1.getPrice())) {
                            return 1;
                        }
                    default:
                        return 0;
                }
            };


            Thread thread = new Thread(() -> {
                Collections.sort(viewModel.getHouses(), comparator);
                if (viewModel.getAdapter() != null)
                    viewModel.getAdapter().notifyDataSetChanged();
            });
            thread.run();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {}

    };

    private void onLogIn() {
        LinearLayout header = (LinearLayout) draverList.getHeaderView(0);
        TextView logIn = header.findViewById(R.id.log_in);
        logIn.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
        });
    }

    private void textChangeAnimation(TextView textView, String text) {
        textView.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_out));
        textView.setText(text);
        textView.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_in));
    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressBar != null) {
            progressBar.setBackgroundResource(android.R.color.transparent);
            root.removeView(progressBar);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        recyclerView.removeOnItemTouchListener(onItemClickListener);
        pager.setVisibility(View.VISIBLE);
        if (viewModel.getOnGetUserAreaListener() != null)
            viewModel.onGetUserAreaListener.onArea();
        toggle.syncState();
    }

    private boolean isLocationEnabled() {
        LocationManager lm = (LocationManager)Objects.requireNonNull(getContext()).getSystemService(Context.LOCATION_SERVICE);
        boolean enabled = false;

        try {
            enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {
            Log.e(TAG, "isLocationEnabled: ", ex );
        }

        try {
            enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {  Log.e(TAG, "isLocationEnabled: ", ex ); }

        return enabled;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(HomeFragmentViewModel.class);

        viewModel.onLoadComplete = () -> swipeRefreshLayout.setRefreshing(false);

        viewModel.beforeBoundariesResult = () -> {
         locationAnimation = (AnimationDrawable) locationLayout.getBackground();
            locationAnimation.setEnterFadeDuration(2000);
            locationAnimation.setExitFadeDuration(2000);
            locationRequestLayout.setVisibility(View.VISIBLE);
            if (viewModel.checkIfAlreadyHavePermission() && isLocationEnabled()) {

                locationMessage.setTextSize(getResources().getDimensionPixelSize(R.dimen.location_message_search_text_size));
                textChangeAnimation(locationMessage, "Пошук...");
                locationAnimation.start();
                viewModel.getLastLocation();
                locationRequestLayout.setClickable(false);
            } else {
                locationMessage.setTextSize(getResources().getDimensionPixelSize(R.dimen.location_message_text_size));
                textChangeAnimation(locationMessage, "Будь ласка, надайте доступ до геолокації");
                locationRequestLayout.setOnClickListener(v -> {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        12124);
                viewModel.beforeBoundariesResult.locationRequest();
                });
            }
            if(!isLocationEnabled()){
                locationRequestLayout.setClickable(true);
                locationRequestLayout.setOnClickListener(v -> {
                    Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(callGPSSettingIntent, 0);
                });
            }
        };
        viewModel.onBoundariesResult = (nearHouses) -> {
            if (!nearHouses.isEmpty()) {
                locationRequestLayout.setVisibility(View.GONE);
                if (locationAnimation != null) locationAnimation.stop();
                if (pager.getAdapter() == null) {
                    try {
                        areaLayout.setVisibility(View.VISIBLE);
                        pager.setClipToPadding(false);
                        pager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.location_proposition_page_margin));
                        int padding = getResources().getDimensionPixelOffset(R.dimen.location_proposition_padding);
                        pager.setPadding(padding, 0, padding, 0);
                        pager.setOffscreenPageLimit(1);
                        pager.setAdapter(viewModel.initNearbyPropAdapter(nearHouses, waiting));
                    } catch (IllegalStateException e) {
                        Log.e(TAG, "onPostExecute: ", e);
                    }
                } else if (viewModel.getNearbyPropAdapter() != null) {
                    viewModel.getNearbyPropAdapter().updateHouses(nearHouses);
                }
            }

            if (viewModel.getNearbyPropAdapter() == null) {
                locationMessage.setTextSize(getResources().getDimensionPixelSize(R.dimen.location_message_text_size));
                locationAnimation.stop();
                textChangeAnimation(locationMessage, "Нічого не знайдено");
            }
            //Close location layout
            new Handler().postDelayed(() -> locationRequestLayout.setVisibility(View.GONE), 2000);
        };
    }

    private View.OnClickListener submitListener = view -> {
        Spinner saleSpinner = root.findViewById(R.id.spinner_house);
        Spinner operationSpinner = root.findViewById(R.id.spinner_operation);

        EditText startPrice = root.findViewById(R.id.start_price);
        EditText endPrice = root.findViewById(R.id.end_price);

        EditText startArea = root.findViewById(R.id.start_area);
        EditText endArea = root.findViewById(R.id.end_area);

        EditText startFloor = root.findViewById(R.id.start_floor);
        EditText endFloor = root.findViewById(R.id.end_floor);

        Filtres.sale_type = ((TextView) saleSpinner.getSelectedView()).getText().toString();
        Filtres.operation_type = ((TextView) operationSpinner.getSelectedView()).getText().toString();

        String startPriceText = startPrice.getText().toString();
        if (!startPriceText.isEmpty()) Filtres.price_start = Double.valueOf(startPriceText);

        String endPriceText = endPrice.getText().toString();
        if (!endPriceText.isEmpty()) Filtres.price_end = Double.valueOf(endPriceText);

        String areaStartText = startArea.getText().toString();
        if (!areaStartText.isEmpty()) Filtres.area_start = Double.valueOf(areaStartText);


        String areaEndText = endArea.getText().toString();
        if (!areaEndText.isEmpty()) Filtres.area_end = Double.valueOf(areaEndText);

        String startFloorText = startFloor.getText().toString();
        if (!startFloorText.isEmpty()) Filtres.floor_start = Integer.valueOf(startFloorText);

        String endFloorText = endFloor.getText().toString();
        if (!endFloorText.isEmpty()) Filtres.floor_end = Integer.valueOf(endFloorText);
        Filtres.rooms = roomsCount.stepper.getValue();
        root.closeDrawer(filterView);
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (DrawerLayout) inflater.inflate(R.layout.home_fragment, container, false);
        pager = root.findViewById(R.id.location_propositions);
        areaLayout = root.findViewById(R.id.area);
        locationLayout = root.findViewById(R.id.location);
        locationRequestLayout = root.findViewById(R.id.enable_location_request);
        draverList = root.findViewById(R.id.nav_view);
        swipeRefreshLayout = root.findViewById(R.id.refresh);
        SearchView searchView = root.findViewById(R.id.search);
        Toolbar toolbar = root.findViewById(R.id.toolbar);
        filterView = root.findViewById(R.id.filter_view);
        roomsCount = root.findViewById(R.id.rooms_count);
        roomsCount.enableSideTap(true);
        Button submitFilter = root.findViewById(R.id.submit_filter);
        Spinner spinner = root.findViewById(R.id.order_by);
        recyclerView = root.findViewById(R.id.propositions_recycler_view);
        ImageButton spanComposer = root.findViewById(R.id.compose_grid);
        ImageButton liked = root.findViewById(R.id.liked);
        locationMessage = root.findViewById(R.id.location_message);

        onLogIn();
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorPrimaryLight);
        swipeRefreshLayout.setOnRefreshListener(() -> swipeRefreshLayout.setRefreshing(viewModel.getHousesFromApi()));

        //load animation
        swipeRefreshLayout.setRefreshing(true);
        viewModel.getHousesFromApi();

        searchView.setOnClickListener(v -> searchView.setIconified(false));

        seekBarInit();

        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        Button filter = root.findViewById(R.id.filter);

        if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            onRequestPermissionsResult(102, new String[] {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, new int[]{123});
        }

        toggle = new ActionBarDrawerToggle(
                getActivity(), root, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        root.addDrawerListener(toggle);
        toolbar.bringToFront();

        roomsCount.stepper.setValue(1);
        roomsCount.stepper.setMin(1);
        roomsCount.stepper.setMax(4);

        submitFilter.setOnClickListener(submitListener);

        spinner.getBackground().setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.order_by, R.layout.spinner_item);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_checked_item);
        spinner.setAdapter(arrayAdapter);

        liked.setOnClickListener(v -> getActivity().getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out).add(R.id.container, new FavouritePropositionsFragment()).addToBackStack("favorite").commit());


        manager = new GridLayoutManager(getContext(), 1);
        spanComposer.setImageResource(R.drawable.ic_view_agenda_white_36dp);
        spanComposer.setOnClickListener(v -> {
            if (spanComposer.isActivated()) {
                ObjectAnimator.ofFloat(spanComposer, "rotation", 90, 0).setDuration(300).start();
                manager.setSpanCount(1);
                spanComposer.setActivated(false);
            } else {
                ObjectAnimator.ofFloat(spanComposer, "rotation", 0, 90).setDuration(300).start();
                manager.setSpanCount(2);
                spanComposer.setActivated(true);
            }
            viewModel.getAdapter().notifyItemRangeChanged(0, viewModel.getAdapter().getItemCount());
        });

        

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(PropositionsAdapter.GRID_ITEM, 8);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(PropositionsAdapter.LINEAR_ITEM, 8);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(PropositionsAdapter.LOADING_ITEM, 1);
        recyclerView.setLayoutManager(manager);
        try {
            recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getContext(), R.anim.todo_layout_animation_waterfall));
        } catch (NullPointerException e) {
            Log.e(TAG, "onCreateView: ",e );
        }
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(viewModel.initAdapter(manager, waiting));
        viewModel.getAdapter().accessToViewHolder = pos -> {
            try {
                return (PropositionsAdapter.ViewHolder) recyclerView.findViewHolderForAdapterPosition(pos);
            } catch (IndexOutOfBoundsException e) {
                Log.e(TAG, "onCreateView: ", e);
                return null;
            }
        };

        spinner.setOnItemSelectedListener(orderByItemClickListener);

       draverList.setNavigationItemSelectedListener(this);
        draverList.setSaveEnabled(false);
        filter.setOnClickListener(v -> {
            if (root.isDrawerOpen(filterView)) {
                root.closeDrawer(filterView);
            } else {
                root.openDrawer(filterView);
            }
        });

        return root;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){

            case R.id.nav_favorite:
                fragment = new FavouritePropositionsFragment();
                fragmentTag = HomeActivity.FAVORITES_FRAGMENT_TAG;
                break;
            case R.id.nav_cabinet:
                fragment = new CabinetFragment();
                fragmentTag = HomeActivity.CABINET_FRAGMENT_TAG;
                break;
            case R.id.nav_public:
                fragment = new NewPostFragment();
                fragmentTag = HomeActivity.PUBLIC_FRAGMENT_TAG;
                break;
            case R.id.nav_log_out : break;
            case R.id.nav_feedback :

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse( "mailto:bob@example.org" +
                        "?subject=feedback" +
                        "&body= Шановний розробнику, "));
                startActivity(Intent.createChooser(emailIntent, "Надіслати з допомогою"));
                break;
        }

        root.closeDrawer(GravityCompat.START);

        if(fragment != null)
        new Handler().postDelayed(() -> {
            getActivity().getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).setCustomAnimations(R.anim.fade_in, R.anim.fade_out).add(R.id.container, fragment, fragmentTag).addToBackStack("fragment").commit();
            fragment = null;
        }, 250);

        return true;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (progressBar != null) {
            progressBar.setBackgroundResource(android.R.color.transparent);
            root.removeView(progressBar);
            recyclerView.setVisibility(View.VISIBLE);
            pager.setVisibility(View.VISIBLE);
        }
    }


    private void seekBarInit() {
        EditText startPrice = root.findViewById(R.id.start_price);
        EditText endPrice = root.findViewById(R.id.end_price);

        startPrice.setText("0");
        endPrice.setText("1000");
        RangeSeekBar rangeSeekBar = root.findViewById(R.id.price_range);

        rangeSeekBar.setRangeValues(0, 1000);
        rangeSeekBar.setOnRangeSeekBarChangeListener((bar, minValue, maxValue) -> {
            startPrice.setText(minValue.toString());
            endPrice.setText(maxValue.toString());
        });

        startPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals("")) {
                    minValue = Integer.valueOf(s.toString());
                    if (minValue < maxValue) {
                        rangeSeekBar.setSelectedMaxValue(minValue);
                        if (minValue < rangeSeekBar.getAbsoluteMinValue().floatValue())
                            rangeSeekBar.setRangeValues(minValue, maxValue);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        endPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals("")) {
                    maxValue = Integer.valueOf(s.toString());
                    if (maxValue > minValue) {
                        rangeSeekBar.setSelectedMaxValue(maxValue);
                        if (maxValue > rangeSeekBar.getAbsoluteMaxValue().floatValue())
                            rangeSeekBar.setRangeValues(minValue, maxValue);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

    }

}

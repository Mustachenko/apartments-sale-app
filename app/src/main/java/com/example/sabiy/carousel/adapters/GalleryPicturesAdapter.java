package com.example.sabiy.carousel.adapters;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.callbacks.PicturesDiffUtillCallback;
import com.example.sabiy.carousel.dataWrapper.GallerySinglePicture;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.refactor.library.SmoothCheckBox;

public class GalleryPicturesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> exportPictures;
    private List<GallerySinglePicture> pictures;
    public OpenFullScreenGallery openFullScreenGallery;

    public OnCameraClickListener onCameraClickListener;

    public GalleryPicturesAdapter(List<GallerySinglePicture> pictures, boolean onePictureMode, ArrayList<String> checked) {
        this.exportPictures = new ArrayList<>();
        this.onePictureMode = onePictureMode;
        GallerySinglePicture cameraPicture = new GallerySinglePicture("", false);
        pictures.add(0, cameraPicture);
        if (checked != null)
        for (GallerySinglePicture picture : pictures) {
            if (checked.contains(picture.getPicture())) {
                picture.setActive(true);
            }
        }
        this.pictures = pictures;
    }


    public void updateItems(List<GallerySinglePicture> pictures) {

        GallerySinglePicture cameraPicture = new GallerySinglePicture("", false);
        pictures.add(0, cameraPicture);
        PicturesDiffUtillCallback callback = new PicturesDiffUtillCallback(this.pictures, pictures);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(callback);
        diffResult.dispatchUpdatesTo(this);
        this.pictures = pictures;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (isOnePictureMode()) {
            SingleViewHolder singleViewHolder = (SingleViewHolder) viewHolder;
            if (pictures.get(i).getPicture().isEmpty()) {
                singleViewHolder.imageView.setImageResource(R.drawable.camera_center_inside);
                singleViewHolder.imageView.setBackgroundResource(R.color.smoky_background);
                singleViewHolder.imageView.setOnClickListener(v -> onCameraClickListener.onCameraClick());
            } else {
                int size = (int) singleViewHolder.imageView.getContext().getResources().getDimension(R.dimen.galleryPictureSize);
                Picasso.get().load(new File(pictures.get(i).getPicture())).centerCrop().resize(singleViewHolder.imageView.getWidth(), size).into(singleViewHolder.imageView);

                singleViewHolder.imageView.setOnClickListener(v-> {
                   int position = viewHolder.getAdapterPosition();
                        exportPictures.add(pictures.get(position).getPicture());
                        pictures.get(position).setActive(true);
                        setUserPicture.set();
                });

            }

        } else {
            MultyViewHolder multyViewHolder = (MultyViewHolder) viewHolder;

        multyViewHolder.imageView = multyViewHolder.view.findViewById(R.id.picture);
            multyViewHolder.checkBox = multyViewHolder.view.findViewById(R.id.check_to_add);
            multyViewHolder.checkBox.bringToFront();

            if (pictures.get(i).isActive()) {
            multyViewHolder.checkBox.setChecked(true);
                if (!exportPictures.contains(pictures.get(i).getPicture())) {
                    exportPictures.add(pictures.get(i).getPicture());
                }
        } else {
            multyViewHolder.checkBox.setChecked(false);
        }

        if(pictures.get(i).getPicture().isEmpty()) {
            multyViewHolder.imageView.setImageResource(R.drawable.camera_center_inside);
            multyViewHolder.checkBox.setVisibility(View.GONE);
            multyViewHolder.imageView.setBackgroundResource(R.color.smoky_background);
            multyViewHolder.imageView.setOnClickListener(v -> onCameraClickListener.onCameraClick());
        } else {
            multyViewHolder.checkBox.setVisibility(View.VISIBLE);
                int size = (int) multyViewHolder.imageView.getContext().getResources().getDimension(R.dimen.galleryPictureSize);
            Picasso.get().load(new File(pictures.get(i).getPicture())).centerCrop().resize(multyViewHolder.imageView.getWidth(), size).into(multyViewHolder.imageView);

            multyViewHolder.checkBox.setOnClickListener(v -> {
                int position = viewHolder.getAdapterPosition();
                if (pictures.get(position).isActive()) {
                    multyViewHolder.checkBox.setChecked(false, true);
                    pictures.get(position).setActive(false);
                    exportPictures.remove(pictures.get(position).getPicture());
                } else {
                    if (onePictureMode) {
                        for (int j = 1; j < pictures.size(); j++) {
                            pictures.get(j).setActive(false);
                            exportPictures.clear();
                        }
                    }
                    multyViewHolder.checkBox.setChecked(true, true);
                    exportPictures.add(pictures.get(position).getPicture());
                    pictures.get(position).setActive(true);
                }
            });

            multyViewHolder.imageView.setOnClickListener(v-> {
                openFullScreenGallery.open(viewHolder.getAdapterPosition() - 1);
                });
        }
        }
    }

    //set true if only one picture required
    private boolean onePictureMode;

    public SetUserPicture setUserPicture;

    public interface SetUserPicture {
        void set();
    }

    public boolean isOnePictureMode() {
        return onePictureMode;
    }

    public List<String> getExportPictures() {
        return exportPictures;
    }

    public List<GallerySinglePicture> getPictures() {
        List<GallerySinglePicture> result = new ArrayList<>(pictures);
        result.remove(0);
        return result;
    }

    public interface OnCameraClickListener {
        void onCameraClick();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;
        if (isOnePictureMode()) {
            ImageView imageView = (ImageView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_single_picture_layout, viewGroup, false);
            viewHolder = new SingleViewHolder(imageView);
        } else {
            RelativeLayout view = (RelativeLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_multy_picture_layout, viewGroup, false);
            viewHolder = new MultyViewHolder(view);
        }
        return viewHolder;
    }

    public interface OpenFullScreenGallery {
        void open(int position);
    }



    @Override
    public int getItemCount() {
        return pictures.size();
    }

    class MultyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout view;
        ImageView imageView;
        SmoothCheckBox checkBox;
        public MultyViewHolder(@NonNull RelativeLayout itemView) {
            super(itemView);
            view = itemView;
        }
    }
    class SingleViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public SingleViewHolder(@NonNull ImageView itemView) {
            super(itemView);
            imageView = itemView;
        }
    }
}

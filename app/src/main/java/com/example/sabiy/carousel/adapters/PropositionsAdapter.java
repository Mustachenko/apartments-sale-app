package com.example.sabiy.carousel.adapters;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.card.MaterialCardView;
import android.support.v7.graphics.Palette;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sabiy.carousel.API.House;
import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.Waiting;
import com.example.sabiy.carousel.callbacks.HousesDiffUtillcallback;
import com.example.sabiy.carousel.views.activities.PropositionsActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class PropositionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String HOUSES = "houses";
    private GridLayoutManager manager;
    public static final int LINEAR_ITEM = 1;
    public static final int LOADING_ITEM = 0;
    public static final int GRID_ITEM = 2;
    public static final String POSITION = "selectedItemPosition";
    private OnLoadMoreListener onLoadMoreListener;
    private List<House> houses;
    public Waiting waiting;
    private ProgressBar progressBar;
    public AccessToViewHolder accessToViewHolder;
    public interface AccessToViewHolder {
        ViewHolder access(int pos);
    }

    public void updatePropositions(List<House> newHouses) {
        houses = newHouses;
        HousesDiffUtillcallback diffUtillcallback = new HousesDiffUtillcallback(houses, newHouses);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(diffUtillcallback, true);
        result.dispatchUpdatesTo(this);

    }

    public PropositionsAdapter(GridLayoutManager manager, List<House> houses) {
        this.manager = manager;
        this.houses = houses;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public interface OnLoadMoreListener {
        void onMore();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        MaterialCardView cardView;
        RelativeLayout relativeLayout;
        RecyclerView.ViewHolder viewHolder = null;
        switch (i) {
            case LOADING_ITEM : relativeLayout = (RelativeLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.loading_proposition, viewGroup, false);
                viewHolder = new LoadingViewHolder(relativeLayout);
                onLoadMoreListener.onMore();
                break;
            case LINEAR_ITEM : cardView = (MaterialCardView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.proposition_layout_linear, viewGroup,false);
                viewHolder = new ViewHolder(cardView);
                break;
            case GRID_ITEM : cardView = (MaterialCardView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.proposition_layout_grid, viewGroup,false);
                viewHolder = new ViewHolder(cardView);
                break;
        }
        return  viewHolder;

    }

    @Override
    public int getItemViewType(int position) {
        if(manager.getSpanCount() == LINEAR_ITEM){
            return LINEAR_ITEM;
        } else if(manager.getSpanCount() == GRID_ITEM) {
            return GRID_ITEM;
        } else if (position == houses.size()-1) {
            return LOADING_ITEM;
        } else return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
            if(viewHolder.getItemViewType() == LINEAR_ITEM || viewHolder.getItemViewType() == GRID_ITEM ) {
                ((ViewHolder) viewHolder).setWaiting(waiting);
                ViewHolder regularViewHolder = (ViewHolder) viewHolder;
                MaterialCardView cardView = regularViewHolder.cardView;
                regularViewHolder.preview = cardView.findViewById(R.id.preview);
                regularViewHolder.label = cardView.findViewById(R.id.label);
                regularViewHolder.container = cardView.findViewById(R.id.container);
                regularViewHolder.price = cardView.findViewById(R.id.prise);
                regularViewHolder.reviews = cardView.findViewById(R.id.reviews);

                regularViewHolder.label.setText(houses.get(i).getLabel());
                regularViewHolder.price.setText(houses.get(i).getPrice()+ " $");
                regularViewHolder.reviews.setText(houses.get(i).getReviews());

                cardView.setOnClickListener(v -> {
                    waiting.startWaiting();
                    Intent intent = new Intent(regularViewHolder.container.getContext(), PropositionsActivity.class);
                    intent.putExtra(PropositionsAdapter.POSITION, viewHolder.getAdapterPosition());
                    intent.putParcelableArrayListExtra(HOUSES, (ArrayList<? extends Parcelable>) houses);
                    regularViewHolder.container.getContext().startActivity(intent);
                });
                progressBar = cardView.findViewById(R.id.image_load_progress);
                Picasso.get().load(houses.get(i).getPreview()).into(new Target() {

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        if (progressBar != null)
                            progressBar.setVisibility(View.GONE);
                        Log.i("", "onBitmapLoaded: bitmap loaded");
                        regularViewHolder.preview.setImageBitmap(bitmap);

                        Palette.from(bitmap)
                                .generate(palette -> {
                                    GradientDrawable imageForegroundGradient = new GradientDrawable();
                                    int pos = viewHolder.getAdapterPosition();
                                    Palette.Swatch vSwatch = palette.getLightVibrantSwatch();
                                    if (vSwatch == null) vSwatch = palette.getLightMutedSwatch();
                                    if (vSwatch == null) vSwatch = palette.getDominantSwatch();
                                    Palette.Swatch lvSwatch = palette.getLightVibrantSwatch();
                                    if (vSwatch != null && lvSwatch != null) {

                                        ViewHolder correctHolder = accessToViewHolder.access(pos);

                                        if (getItemViewType(viewHolder.getAdapterPosition()) == GRID_ITEM) {
                                            imageForegroundGradient.setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
                                            if (correctHolder != null) correctHolder.container.setBackground(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{vSwatch.getRgb(), lvSwatch.getRgb()}));

                                        } else if (getItemViewType(viewHolder.getAdapterPosition()) == LINEAR_ITEM) {
                                            imageForegroundGradient.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);
                                            if (correctHolder != null) correctHolder.container.setBackground(new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{vSwatch.getRgb(), lvSwatch.getRgb()}));
                                        }
                                        imageForegroundGradient.setColors(new int[] {cardView.getResources().getColor(android.R.color.transparent), cardView.getResources().getColor(android.R.color.transparent), vSwatch.getRgb()});
                                        if (correctHolder != null && Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) correctHolder.preview.setForeground(imageForegroundGradient);
                                        regularViewHolder.price.setTextColor(vSwatch.getTitleTextColor());
                                        regularViewHolder.label.setTextColor(vSwatch.getBodyTextColor());
                                        regularViewHolder.reviews.setTextColor(vSwatch.getBodyTextColor());
                                        regularViewHolder.container.bringToFront();
                                    }
                                });
                    }
                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                    }
                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        if (progressBar != null)
                            progressBar.setVisibility(View.VISIBLE);

                    }
                });
            }
            else {
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewHolder;
                RelativeLayout relativeLayout = loadingViewHolder.relativeLayout;
                loadingViewHolder.indicatorView = relativeLayout.findViewById(R.id.indicator);
                loadingViewHolder.indicatorView.smoothToShow();
            }

    }

    @Override
    public int getItemCount() {
        return houses.size();
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout;
        AVLoadingIndicatorView indicatorView;
        LoadingViewHolder(@NonNull RelativeLayout itemView) {
            super(itemView);
            relativeLayout = itemView;
        }
    }

     public static class ViewHolder extends RecyclerView.ViewHolder {
        MaterialCardView cardView;
        ImageView preview;
        TextView price;
         View container;
        TextView label;
        TextView reviews;
         Waiting waiting;

         ViewHolder(@NonNull MaterialCardView itemView) {
            super(itemView);
            cardView = itemView;

        }

         public void setWaiting(Waiting waiting) {
             this.waiting = waiting;
         }
    }
}

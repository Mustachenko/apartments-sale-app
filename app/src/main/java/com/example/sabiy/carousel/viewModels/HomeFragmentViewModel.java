package com.example.sabiy.carousel.viewModels;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;

import com.example.sabiy.carousel.API.Apartments;
import com.example.sabiy.carousel.API.ApartmentsApi;
import com.example.sabiy.carousel.API.GeocodingLocation;
import com.example.sabiy.carousel.API.House;
import com.example.sabiy.carousel.Waiting;
import com.example.sabiy.carousel.adapters.NearbyPropAdapter;
import com.example.sabiy.carousel.adapters.PropositionsAdapter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.support.constraint.Constraints.TAG;


public class HomeFragmentViewModel extends AndroidViewModel {
    public OnGetUserAreaListener onGetUserAreaListener;
    public OnBoundariesResult onBoundariesResult;
    public BeforeBoundariesResult beforeBoundariesResult;

    public interface BeforeBoundariesResult {
        void locationRequest();
    }
    public OnLoadComplete onLoadComplete;

    private LocationRequest mLocationRequest;

    private PropositionsAdapter adapter;
    private LatLngBounds area;
    private ApartmentsApi.ApiInterfaceProp api;

    public HomeFragmentViewModel(@NonNull Application application) {
        super(application);
        setLayoutRequest();
    }

    private List<House> houses = new ArrayList<>();
    private NearbyPropAdapter nearbyPropAdapter;

    public NearbyPropAdapter getNearbyPropAdapter() {
        return nearbyPropAdapter;
    }

    public boolean getHousesFromApi() {
        api = ApartmentsApi.getClient(getApplication(), ApartmentsApi.BASE_URL).create(ApartmentsApi.ApiInterfaceProp.class);
        Observable<Apartments> apartmentsObservable = api.getPropositions();

        apartmentsObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Observer<Apartments>() {

                              @Override
                              public void onSubscribe(Disposable d) {
                              }

                              @Override
                              public void onNext(Apartments apartments) {
                                  List<House> newHouses = apartments.getHouses();
                                  adapter.updatePropositions(newHouses);
                                  houses = newHouses;
                                  if (onGetUserAreaListener != null)
                                      onGetUserAreaListener.onArea();
                              }

                              @Override
                              public void onError(Throwable e) {
                                  e.printStackTrace();
                                  onLoadComplete.onComplete();
                              }

                              @Override
                              public void onComplete() {
                                  beforeBoundariesResult.locationRequest();
                                  onLoadComplete.onComplete();
                              }
                          }
                );
        return true;

    }

    private void setLayoutRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setMaxWaitTime(8000);
        mLocationRequest.setInterval(6000 * 60);
        mLocationRequest.setFastestInterval(4000 * 60);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    public interface OnLoadComplete { void onComplete();}

    public OnGetUserAreaListener getOnGetUserAreaListener() {
        return onGetUserAreaListener;
    }

    public LatLngBounds getArea() {
        return area;
    }

    public void setArea(LatLngBounds area) {
        this.area = area;
    }

    public ApartmentsApi.ApiInterfaceProp getApi() {
        return api;
    }

    public void setApi(ApartmentsApi.ApiInterfaceProp api) {
        this.api = api;
    }

    public PropositionsAdapter getAdapter() {
        return adapter;
    }

    public PropositionsAdapter initAdapter(GridLayoutManager manager, Waiting waiting) {
        if (adapter == null) {
            adapter = new PropositionsAdapter(manager, houses);
            adapter.waiting = waiting;
        }
        return adapter;
    }

    public List<House> getHouses() {
        return houses;
    }

    public NearbyPropAdapter initNearbyPropAdapter(List<House> nearHouses, Waiting waiting) {

        if (nearbyPropAdapter == null) {
            nearbyPropAdapter = new NearbyPropAdapter(nearHouses);
            nearbyPropAdapter.waiting = waiting;
        }
        return nearbyPropAdapter;
    }


    public boolean checkIfAlreadyHavePermission() {
        int result = ContextCompat.checkSelfPermission(Objects.requireNonNull(getApplication()), Manifest.permission.ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressLint("MissingPermission")
    public void getLastLocation() {


        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(Objects.requireNonNull(getApplication()));

        fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, new LocationCallback() {
            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
                if (!locationAvailability.isLocationAvailable()) {
                    new AsyncFindInArea().execute(houses.toArray(new House[houses.size()]));
                }
            }

            @Override
            public void onLocationResult(LocationResult locationResult) {
                Location location = locationResult.getLastLocation();
                area = new LatLngBounds(new LatLng(location.getLatitude() - 0.0025f, location.getLongitude() - 0.0025f), new LatLng(location.getLatitude() + 0.0025f, location.getLongitude() + 0.0025f));
                new AsyncFindInArea().execute(houses.toArray(new House[houses.size()]));

                //    onGetUserAreaListener = () -> new AsyncFindInArea().execute(houses.toArray(new House[houses.size()]));
            }
        }, Looper.myLooper());
    }

    public interface OnGetUserAreaListener {
        void onArea();
    }

    public interface OnBoundariesResult {
        void onResult(List<House> nearHouses);
    }

    @SuppressLint("StaticFieldLeak")
    class AsyncFindInArea extends AsyncTask<House, Void, List<House>> {

        @Override
        protected final List<House> doInBackground(House... houses) {
            List<House> localHouses = new ArrayList<>();
            if (area != null) {
                try {
                    for (House house : houses) {
                        if (area.contains(GeocodingLocation.getAddressFromLocation(house.getAdress(),
                                getApplication())) && localHouses.size() < 8) {
                            localHouses.add(house);
                        }
                    }
                } catch (NullPointerException e) {
                    Log.w(TAG, "doInBackground: Can`t load location data");
                }
            }
            return localHouses;
        }

        @Override
        protected void onPostExecute(List<House> houses) {
            super.onPostExecute(houses);
                onBoundariesResult.onResult(houses);
        }
    }

}

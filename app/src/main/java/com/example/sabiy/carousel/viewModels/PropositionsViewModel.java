package com.example.sabiy.carousel.viewModels;

import android.arch.lifecycle.ViewModel;
import android.support.v4.app.FragmentManager;

import com.example.sabiy.carousel.API.House;
import com.example.sabiy.carousel.adapters.ApartmentDataAdapter;

import java.util.List;

public class PropositionsViewModel extends ViewModel {
    private List<House> houses;
    private ApartmentDataAdapter adapter;

    public void setHouses(List<House> houses) {
        this.houses = houses;
    }

    public ApartmentDataAdapter getAdapter() {
        return adapter;
    }

    public ApartmentDataAdapter initAdapter(FragmentManager fragmentManager) {
        adapter = null;
        adapter = new ApartmentDataAdapter(fragmentManager, houses);
        return adapter;
    }
}

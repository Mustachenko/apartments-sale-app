package com.example.sabiy.carousel.dataWrapper;

import android.os.Parcel;
import android.os.Parcelable;

public class Seller implements Parcelable {
    public static final Creator<Seller> CREATOR = new Creator<Seller>() {
        @Override
        public Seller createFromParcel(Parcel in) {
            return new Seller(in);
        }

        @Override
        public Seller[] newArray(int size) {
            return new Seller[size];
        }
    };
    public String name;
    public String phone_number1;
    public String phone_number2;
    public String email;

    protected Seller(Parcel in) {
        name = in.readString();
        phone_number1 = in.readString();
        phone_number2 = in.readString();
        email = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(phone_number1);
        dest.writeString(phone_number2);
        dest.writeString(email);
    }
}
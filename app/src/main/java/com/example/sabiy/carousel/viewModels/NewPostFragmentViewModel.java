package com.example.sabiy.carousel.viewModels;

import android.Manifest;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.example.sabiy.carousel.API.ApartmentsApi;
import com.example.sabiy.carousel.API.GeocodingLocation;
import com.example.sabiy.carousel.API.PostHouse;
import com.example.sabiy.carousel.adapters.FromCameraPicturesAdapter;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class NewPostFragmentViewModel extends AndroidViewModel {
    private FromCameraPicturesAdapter adapter;
    private LatLng latLng;
    private PostHouse postHouse;
    private String address = "";
    private ArrayList<String> pictures;
    public CheckFields checkFields;

    public void setPostHouse(PostHouse postHouse) {
        this.postHouse = postHouse;
    }

    public String getAddress() {
        return address;
    }

    public interface CheckFields {
        boolean check();
    }


    public boolean checkIfAlreadyHavePermission() {
        int result = ContextCompat.checkSelfPermission(Objects.requireNonNull(getApplication()), Manifest.permission.READ_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    public NewPostFragmentViewModel(@NonNull Application application) {
        super(application);
        pictures = new ArrayList<>();
    }

    public boolean setmarker(String address) {
        latLng = (GeocodingLocation.getAddressFromLocation(address, getApplication()));
        return true;
    }

    public void createNewPost() {
        try {
            if (latLng != null) {
                Geocoder geocoder = new Geocoder(getApplication(), Locale.getDefault());
                Address addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1).get(0);
                address = addressList.getFeatureName() + ", " + addressList.getSubAdminArea() + ", " + addressList.getLocality() + ", " +  addressList.getAdminArea()  + ", " + addressList.getCountryName() + ", " + addressList.getCountryCode();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        MultipartBody.Part[] images = new MultipartBody.Part[adapter.getPictures().size()];
        for (int index = 0; index < adapter.getPictures().size(); index++) {
            File file = new File(adapter.getPictures().get(index));
            RequestBody body = RequestBody.create(MediaType.parse("image/*"), file);
            images[index] = MultipartBody.Part.createFormData("picture", file.getName(), body);
        }

        if (adapter.getPreview() != null && checkFields.check()) {
            File file = new File(adapter.getPreview());
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");
            ApartmentsApi.ApiInterfaceApart api = ApartmentsApi.getClient(getApplication(), ApartmentsApi.MISHA_URL).create(ApartmentsApi.ApiInterfaceApart.class);
            try {
                Flowable<ResponseBody> sendCall = api.sendNewHouse(postHouse);
                sendCall.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(response -> {
                    Toast.makeText(getApplication(), "Опубліковано", Toast.LENGTH_LONG).show();
                }).isDisposed();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }

            //, body, name, images

        }

    }


    public FromCameraPicturesAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(FromCameraPicturesAdapter adapter) {
        this.adapter = adapter;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public ArrayList<String> getPictures() {
        return pictures;
    }

    public void setPictures(ArrayList<String> pictures) {
        this.pictures = pictures;
    }
}

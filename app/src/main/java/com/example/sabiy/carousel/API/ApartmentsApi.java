package com.example.sabiy.carousel.API;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.atomic.AtomicBoolean;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public class ApartmentsApi {
    private static Retrofit retrofit;

    public static final String BASE_URL = "https://app.fakejson.com/q/";
    public static final String MISHA_URL = "http://crm.webstudia.if.ua/api/";

    public static Retrofit getClient(Context context, String url) {
        //  if (retrofit == null) {
            long cashSize = (long) 5 * 1024 * 1024;
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            Cache cache = new Cache(context.getCacheDir(), cashSize);
            OkHttpClient client = new OkHttpClient.Builder().cache(cache).addInterceptor(logging)
                    .build();


        Gson gson = new GsonBuilder().setLenient()
                .create();
        retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create(gson)).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).client(client).build();


        //   }

        return retrofit;
    }

    public interface ApiInterfaceApart {
        @POST("apartments")
        Flowable<ResponseBody> sendNewHouse(
                @Body PostHouse postHouse
                // @PartMap Map<String,String> queryMap,
                // @PartMap Map<String,Integer> intqueryMap
//                @Field("label") String label,
//               @Field("contact")  String con,
//@Field("id") Integer id,
//@Field("user_id") Integer user,
//
//                @Field("price") Integer prise,
//               @Field("tel1") Integer tel1,
//               @Field("tel2") Integer tel2,
//@Field("desc") String desc,
//              @Field("street") String st,
//               @Field("email") String email,
//                 @Field("active") Integer act,
//
//
////                @Field("house_type") String house_type,
////                @Field("operation_type") String operation_type,
////
//                @Field("area") Integer area,
//         @Field("rooms") Integer rooms,
////                @Field("floors") Integer floors,
//@Field("floor") Integer floor,
//               @Field("holder") Integer holder
//                 @Field("address") String address,
                //        @Field MultipartBody.Part image,
                //        @Field("preview_name") RequestBody name,
                //       @Field MultipartBody.Part[] images
        );
    }

    public interface ApiInterfaceProp {

        @GET("ZEseQfNg?token=6LpN8Q0WnR-zQRD1wPtCcg")
        Observable<Apartments> getPropositions(

                // @Query("sale_type") String sale_type,
                // @Query("operation_type") String operation_type,
                // @Query("building_type") String building_type,
                // @Query("price_start") Double price_start,
                // @Query("price_end") Double price_end,
                // @Query("area_start") Double area_start,
                // @Query("area_start") Double area_end,
                // @Query("rooms") Integer rooms,
                // @Query("floors") Integer floors,
                // @Query("floor_start") Integer floor_start,
                // @Query("floor_end") Integer floor_end,
                // @Query("start_from_id") Integer start_from
        );


    }

    public class Response {


    }

    private static boolean hasNetwork(Context context) {
        AtomicBoolean isConnected = new AtomicBoolean(false);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()) {
            isConnected.set(true);
        }
        return isConnected.get();
    }
}

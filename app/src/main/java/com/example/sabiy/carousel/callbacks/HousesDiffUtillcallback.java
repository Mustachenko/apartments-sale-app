package com.example.sabiy.carousel.callbacks;

import android.support.v7.util.DiffUtil;

import com.example.sabiy.carousel.API.House;

import java.util.List;

public class HousesDiffUtillcallback extends DiffUtil.Callback {
    private List<House> oldHouses;
    private List<House> newHouses;

    public HousesDiffUtillcallback(List<House> oldHouses, List<House> newHouses) {
        this.oldHouses = oldHouses;
        this.newHouses = newHouses;
    }

    @Override
    public int getOldListSize() {
        return oldHouses.size();
    }

    @Override
    public int getNewListSize() {
        return newHouses.size();
    }

    @Override
    public boolean areItemsTheSame(int i, int i1) {
        return oldHouses.get(i).getPreview().equals(newHouses.get(i1).getPreview());
    }

    @Override
    public boolean areContentsTheSame(int i, int i1) {
        return oldHouses.get(i).getPreview().equals(newHouses.get(i1).getPreview()) && oldHouses.get(i).getReviews().equals(newHouses.get(i1).getReviews()) && oldHouses.get(i).getPrice() == newHouses.get(i1).getPrice();
    }
}

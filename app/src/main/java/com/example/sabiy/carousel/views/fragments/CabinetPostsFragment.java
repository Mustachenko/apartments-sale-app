package com.example.sabiy.carousel.views.fragments;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.adapters.UserPostsAdapter;


public class CabinetPostsFragment extends BaseFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ConstraintLayout rootLayout = (ConstraintLayout) inflater.inflate(R.layout.fragment_cabinet_posts, container, false);
        RecyclerView recyclerView = rootLayout.findViewById(R.id.posts);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new UserPostsAdapter());
        FloatingActionButton add = rootLayout.findViewById(R.id.add_new_post);
        add.setOnClickListener(v -> {
            NewPostFragment fragment = new NewPostFragment();
            Bundle args = new Bundle();
            args.putBoolean(NewPostFragment.ARGS, true);
            fragment.setArguments(args);
            getActivity().getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).setCustomAnimations(R.anim.fade_in, R.anim.fade_out).addToBackStack("new post").commit();
        });
        return rootLayout;
    }

}

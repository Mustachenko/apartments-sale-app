package com.example.sabiy.carousel.views.activities;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.views.fragments.HomeFragment;

import java.util.List;

public class HomeActivity extends AppCompatActivity {
    public static final String HOME_FRAGMENT_TAG = "Home";
    public static final String FAVORITES_FRAGMENT_TAG = "Favorites";
    public static final String PUBLIC_FRAGMENT_TAG = "Public";
    public static final String CABINET_FRAGMENT_TAG = "Cabinet";
    public static final String LOG_OUT_FRAGMENT_TAG = "LogOut";

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 106);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            //  if (!checkIfAlreadyHavePermission()) {
            requestForSpecificPermission();
            //}
        }
        String fragmentTag = "";
        if (savedInstanceState != null) {
            fragmentTag = savedInstanceState.getString("fragment");
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentTag == "")
            fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment(), HOME_FRAGMENT_TAG).commit();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        outState.putString("fragment", getSupportFragmentManager().getFragments().get(fragments.size() - 1).getTag());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        return true;
    }

}

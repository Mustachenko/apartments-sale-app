package com.example.sabiy.carousel.views.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.viewModels.GalleryViewModel;
import com.rd.PageIndicatorView;
import com.rd.animation.type.AnimationType;

import java.util.ArrayList;


public class GalleryActivity extends AppCompatActivity {
    public static final String SELECTABLE = "selectable";
    public static final int REQUEST_CODE = 255;
    private GalleryViewModel viewModel;

    private View.OnClickListener onClickListener = (view -> {
        if(getSupportFragmentManager().getBackStackEntryCount()> 0)
            getSupportFragmentManager().popBackStack();
        else onBackPressed();
    });

    @Override
    public void onBackPressed() {
        if (viewModel.getSelectablePicturesPagerAdapter() != null) {
            Intent intent = new Intent();
            intent.putParcelableArrayListExtra(GridGalleryActivity.PICTURES, (ArrayList<? extends Parcelable>) viewModel.getSelectablePicturesPagerAdapter().getPictures());
            setResult(REQUEST_CODE, intent);
            finish();
        } else super.onBackPressed();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_galery);

        RelativeLayout root = findViewById(R.id.layout);
        ImageButton backButton = findViewById(R.id.back_button);
        FrameLayout container = findViewById(R.id.container);
        ViewPager viewPager = findViewById(R.id.viewpager);

        container.bringToFront();
        backButton.bringToFront();
        backButton.setOnClickListener(onClickListener);
        root.setBackgroundColor(Color.BLACK);

        Bundle bundle = getIntent().getExtras();
        int position = bundle != null ? bundle.getInt("position") : 0;
        boolean isSelectable = bundle.getBoolean(SELECTABLE, false);

        if (isSelectable) {
            viewModel.setCheckablePictures(bundle.getParcelableArrayList(GridGalleryActivity.PICTURES));
            viewPager.setAdapter(viewModel.initSelectablePicturesPagerAdapter());
        } else {
            viewModel.setPictures(bundle.getStringArrayList("pictures"));
            viewPager.setAdapter(viewModel.initPicturesPagerAdapter());

            PageIndicatorView pageIndicatorView = new PageIndicatorView(this);
            pageIndicatorView.setAnimationType(AnimationType.WORM);
            pageIndicatorView.setViewPager(viewPager);
            pageIndicatorView.setRadius(getResources().getDimension(R.dimen.piv_radius));
            pageIndicatorView.setPadding(getResources().getDimension(R.dimen.piv_padding));

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.margin);
            params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.margin);
            params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.margin);
            params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
            params.alignWithParent = true;
            params.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.viewpager);

            pageIndicatorView.setLayoutParams(params);
            pageIndicatorView.setDynamicCount(true);
            pageIndicatorView.setDynamicCount(true);
            pageIndicatorView.setVisibility(View.VISIBLE);
            pageIndicatorView.bringToFront();

            root.addView(pageIndicatorView, 0);
        }
        viewPager.setCurrentItem(position);
      }

}

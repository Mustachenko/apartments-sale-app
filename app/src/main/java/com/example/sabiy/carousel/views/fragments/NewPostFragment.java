package com.example.sabiy.carousel.views.fragments;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.sabiy.carousel.API.PostHouse;
import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.adapters.FromCameraPicturesAdapter;
import com.example.sabiy.carousel.viewModels.NewPostFragmentViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.LinkedHashMap;

public class NewPostFragment extends BaseFragment {
    public static final String ARGS = "embedded";
    public static final int NEW_PHOTO_RESULT_CODE = 3246;

    private String address = "";
    private MapView mapView;

    private NewPostFragmentViewModel viewModel;
    private EditText label;
    private EditText descriptionTextView;
    private Spinner house;
    private Spinner operation;
    private EditText area;
    private EditText price;
    private EditText rooms;
    private EditText floor;
    private EditText floors;
    private  EditText location;

    View.OnClickListener submitListener = v -> {


            LinkedHashMap<String,String> map = new LinkedHashMap<>();
        //map.put("label",  label.getText().toString());
        //map.put("price",  price.getText().toString());

        //  map.put("description",  descriptionTextView.getText().toString());
        map.put("contact", descriptionTextView.getText().toString());
        //  map.put("house_type",  ((TextView) house.getSelectedView()).getText().toString());
        //   map.put("operation_type",    ((TextView) operation.getSelectedView()).getText().toString());
            map.put("area",  area.getText().toString());
            map.put("rooms",  rooms.getText().toString());
        //    map.put("floors",  floors.getText().toString());
            map.put("floor",  floor.getText().toString());
        //   map.put("address",   address);
        LinkedHashMap<String, Integer> intmap = new LinkedHashMap<>();

        map.put("street", "Нова");
        map.put("desc", "Опис");
        intmap.put("id", 134);
        intmap.put("price", 412);

        intmap.put("user_id", 562);
        intmap.put("tel1", 22);
        intmap.put("tel2", 22);
        intmap.put("email", 380995526);
        intmap.put("active", 1);
        intmap.put("area", 50);
        intmap.put("rooms", 2);
        intmap.put("floor", 2);
        intmap.put("holder", 1);

        viewModel.createNewPost();

    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onCreate(Bundle save) {
        super.onCreate(save);
        viewModel = ViewModelProviders.of(this).get(NewPostFragmentViewModel.class);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LinearLayout root = (LinearLayout) inflater.inflate(R.layout.fragment_new_post, container, false);
        FloatingActionButton delete = root.findViewById(R.id.delete);
        delete.bringToFront();
        FloatingActionButton cancel = root.findViewById(R.id.cancel);
        cancel.bringToFront();
        if (!viewModel.checkIfAlreadyHavePermission()) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    121);
        }
        ImageView preview = root.findViewById(R.id.preview);

        if (viewModel.getAdapter() != null)
            if (!viewModel.getAdapter().getPreview().equals(""))
                Picasso.get().load(new File(viewModel.getAdapter().getPreview())).into(preview);
        Toolbar toolbar = root.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        location = root.findViewById(R.id.location);
        mapView = root.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onSaveInstanceState(savedInstanceState);

        OnMapReadyCallback onMapReadyCallback = googleMap -> {
            Thread thread = new Thread(() -> {
                if (!address.isEmpty()) {

                    if (viewModel.setmarker(address)) {
                        getActivity().runOnUiThread(() -> {
                            googleMap.clear();
                            if (viewModel.getLatLng() != null) {
                                googleMap.addMarker(new MarkerOptions().position(viewModel.getLatLng()).title(address));
                                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(viewModel.getLatLng(), 15));
                            }
                        });
                    }
                }
            });

            if (!location.getText().toString().isEmpty()) {
                address = location.getText().toString();
                thread.run();
            }
            location.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    ((TransitionDrawable)location.getBackground()).resetTransition();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    address = s.toString();
                    thread.run();
                }
            });

        };
        mapView.getMapAsync(onMapReadyCallback);

        RecyclerView recyclerView = root.findViewById(R.id.pictures);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);


        viewModel.setAdapter(new FromCameraPicturesAdapter(root, viewModel.getPictures()));

        viewModel.checkFields = () -> {
            boolean result = true;
          if ( address.isEmpty()) {
              TransitionDrawable transition = (TransitionDrawable)   location.getBackground();
              transition.startTransition(400);
              new Handler().postDelayed(() -> transition.reverseTransition(400), 400);
              result = false;
          }
          if (viewModel.getAdapter().getPreview().isEmpty()) {
              Toast.makeText(getContext(), "Виберіть головну світлину", Toast.LENGTH_LONG).show();
              result = false;
          }
          if (label.getText().toString().isEmpty()) {
              TransitionDrawable transition = (TransitionDrawable) label.getBackground();
              transition.startTransition(400);
              new Handler().postDelayed(() -> transition.reverseTransition(400), 400);
              result = false;
          }
          if (price.getText().toString().isEmpty()) {
              TransitionDrawable transition = (TransitionDrawable)   price.getBackground();
              transition.startTransition(400);
              new Handler().postDelayed(() -> transition.reverseTransition(400), 400);
              result = false;
          }
          if (area.getText().toString().isEmpty()) {
              TransitionDrawable transition = (TransitionDrawable)   area.getBackground();
              transition.startTransition(400);
              new Handler().postDelayed(() -> transition.reverseTransition(400), 400);
              result = false;
          }
          if (floor.getText().toString().isEmpty()) {
              TransitionDrawable transition = (TransitionDrawable)   floor.getBackground();
              transition.startTransition(400);
              new Handler().postDelayed(() -> transition.reverseTransition(400), 400);
              result = false;
          }

            if (floors.getText().toString().isEmpty()) {
                TransitionDrawable transition = (TransitionDrawable)   floors.getBackground();
                transition.startTransition(400);
                new Handler().postDelayed(() -> transition.reverseTransition(400), 400);
                result = false;
            }

            if (rooms.getText().toString().isEmpty()) {
                TransitionDrawable transition = (TransitionDrawable) rooms.getBackground();
                transition.startTransition(400);
                new Handler().postDelayed(() -> transition.reverseTransition(400), 400);
                result = false;
            }
            viewModel.setPostHouse(new PostHouse(633, viewModel.getAddress(), descriptionTextView.getText().toString(), 23523, "Тест", 234234, 234234, "23456789", 1, Integer.valueOf(area.getText().toString()), Integer.valueOf(rooms.getText().toString()), Integer.valueOf(floor.getText().toString()), Integer.valueOf(floors.getText().toString()), 1));
            return result;
        };

        recyclerView.setAdapter(viewModel.getAdapter());

        label = root.findViewById(R.id.label);
        descriptionTextView = root.findViewById(R.id.description);
        house = root.findViewById(R.id.spinner_house);
        operation = root.findViewById(R.id.spinner_operation);
        area = root.findViewById(R.id.area);
        price = root.findViewById(R.id.price);
        rooms = root.findViewById(R.id.rooms_count);
        floor = root.findViewById(R.id.floor);
        floors = root.findViewById(R.id.floors);

        Button submitButton = root.findViewById(R.id.submit);
        submitButton.setOnClickListener(submitListener);

        return root;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == NEW_PHOTO_RESULT_CODE ) {
            viewModel.setPictures(data.getStringArrayListExtra(("data")));
            viewModel.getAdapter().addNewPicture(viewModel.getPictures());
        }
    }
}

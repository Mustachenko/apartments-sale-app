package com.example.sabiy.carousel.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.sabiy.carousel.views.fragments.CabinetPassportFragment;
import com.example.sabiy.carousel.views.fragments.CabinetPostsFragment;

public class UserDataPagerAdapter extends FragmentPagerAdapter {

    public UserDataPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0 : fragment = new CabinetPassportFragment(); break;
            case 1 : fragment = new CabinetPostsFragment(); break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
package com.example.sabiy.carousel.dataWrapper;

import android.os.Parcel;
import android.os.Parcelable;

public class HouseParameters implements Parcelable {
    public static final Creator<HouseParameters> CREATOR = new Creator<HouseParameters>() {
        @Override
        public HouseParameters createFromParcel(Parcel in) {
            return new HouseParameters(in);
        }

        @Override
        public HouseParameters[] newArray(int size) {
            return new HouseParameters[size];
        }
    };
    public String sale_type;
    public String operation_type;
    public String building_type;
    public Double area;
    public Integer rooms;
    public Integer floors;
    public Integer floor;
    public String address;

    protected HouseParameters(Parcel in) {
        sale_type = in.readString();
        operation_type = in.readString();
        building_type = in.readString();
        if (in.readByte() == 0) {
            area = null;
        } else {
            area = in.readDouble();
        }
        if (in.readByte() == 0) {
            rooms = null;
        } else {
            rooms = in.readInt();
        }
        if (in.readByte() == 0) {
            floors = null;
        } else {
            floors = in.readInt();
        }
        if (in.readByte() == 0) {
            floor = null;
        } else {
            floor = in.readInt();
        }
        address = in.readString();
    }

    public void setSale_type(String sale_type) {
        this.sale_type = sale_type;
    }

    public void setOperation_type(String operation_type) {
        this.operation_type = operation_type;
    }

    public void setBuilding_type(String building_type) {
        this.building_type = building_type;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public void setRooms(Integer rooms) {
        this.rooms = rooms;
    }

    public void setFloors(Integer floors) {
        this.floors = floors;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sale_type);
        dest.writeString(operation_type);
        dest.writeString(building_type);
        if (area == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(area);
        }
        if (rooms == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(rooms);
        }
        if (floors == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(floors);
        }
        if (floor == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(floor);
        }
        dest.writeString(address);
    }
}
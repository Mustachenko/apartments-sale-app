package com.example.sabiy.carousel.dataWrapper;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class SellersHouseDescription implements Parcelable {
    public static final Creator<SellersHouseDescription> CREATOR = new Creator<SellersHouseDescription>() {
        @Override
        public SellersHouseDescription createFromParcel(Parcel in) {
            return new SellersHouseDescription(in);
        }

        @Override
        public SellersHouseDescription[] newArray(int size) {
            return new SellersHouseDescription[size];
        }
    };
    public String label;
    public String preview;
    public Double prise;
    public String desc;
    public List<String> pictures;

    protected SellersHouseDescription(Parcel in) {
        label = in.readString();
        preview = in.readString();
        if (in.readByte() == 0) {
            prise = null;
        } else {
            prise = in.readDouble();
        }
        desc = in.readString();
        pictures = in.createStringArrayList();
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public void setPrise(Double prise) {
        this.prise = prise;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeString(preview);
        if (prise == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(prise);
        }
        dest.writeString(desc);
        dest.writeStringList(pictures);
    }
}
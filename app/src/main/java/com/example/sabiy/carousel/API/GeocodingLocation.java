package com.example.sabiy.carousel.API;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.sabiy.carousel.views.fragments.ApartmentsDataFragment;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GeocodingLocation {
    private static final String TAG = "GeocodingLocation";

    public static LatLng getAddressFromLocation(final String locationAddress,
                                                final Context context) {
        final LatLng[]   latLng = new LatLng[1];

                double latitude = 0;
                double longitude = 0;
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());

                try {
                    List addressList = geocoder.getFromLocationName(locationAddress, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = (Address) addressList.get(0);

                       latitude = address.getLatitude();
                       longitude = address.getLongitude();

                    }
                } catch (IOException e) {
                    Log.e(TAG, "Unable to connect to Geocoder", e);
                } finally {

                    if (latitude != 0) {
                        latLng[0] = new LatLng(latitude, longitude);
                    }
                }
        return latLng[0];
    }
}

package com.example.sabiy.carousel.views.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.views.activities.EditCabinetPassportActivity;

import de.hdodenhof.circleimageview.CircleImageView;



public class CabinetPassportFragment extends BaseFragment {




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ConstraintLayout root =(ConstraintLayout) inflater.inflate(R.layout.fragment_cabinet_passport, container, false);
        CircleImageView userPhoto = root.findViewById(R.id.circleImageView);
        userPhoto.bringToFront();
        FloatingActionButton edit = root.findViewById(R.id.edit_user);
        edit.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), EditCabinetPassportActivity.class));
        });
        return root;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}

package com.example.sabiy.carousel.viewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.sabiy.carousel.adapters.GalleryPicturesAdapter;
import com.example.sabiy.carousel.dataWrapper.GallerySinglePicture;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

public class GridGalleryViewModel extends AndroidViewModel {
    private GalleryPicturesAdapter adapter;
    private String mCurrentPhotoPath;


    public GridGalleryViewModel(@NonNull Application application) {
        super(application);
    }

    public GalleryPicturesAdapter getAdapter() {
        return adapter;
    }

    public GalleryPicturesAdapter initAdapter(boolean mode, ArrayList<String> checkedPictures) {
        if (adapter == null)
            adapter = new GalleryPicturesAdapter(getFilePaths(), mode, checkedPictures);
        return adapter;
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        String imageFileName = "JPEG_";
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/Pictures");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private ArrayList<GallerySinglePicture> getFilePaths() {
        Uri u = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Images.ImageColumns.DATA};
        Cursor c = null;
        SortedSet<String> dirList = new TreeSet<>();
        ArrayList<GallerySinglePicture> resultIAV = new ArrayList<>();

        String[] directories = null;
        if (u != null) {
            c = getApplication().getContentResolver().query(u, projection, null, null, null);
        }

        if ((c != null) && (c.moveToFirst())) {
            do {
                String tempDir = c.getString(0);
                tempDir = tempDir.substring(0, tempDir.lastIndexOf("/"));
                try {
                    dirList.add(tempDir);
                } catch (Exception e) {
                    Log.e(getClass().getName(), "getFilePaths: ", e);
                }
            }
            while (c.moveToNext());
            directories = new String[dirList.size()];
            dirList.toArray(directories);
            c.close();

        }

        for (int i = 0; i < dirList.size(); i++) {
            File imageDir = null;
            if (directories != null) {
                imageDir = new File(directories[i]);
            }
            File[] imageList = imageDir != null ? imageDir.listFiles() : new File[0];
            if (imageList == null)
                continue;
            for (File imagePath : imageList) {
                try {
                    if (imagePath.getName().contains(".jpg") || imagePath.getName().contains(".JPG")
                            || imagePath.getName().contains(".jpeg") || imagePath.getName().contains(".JPEG")
                            || imagePath.getName().contains(".png") || imagePath.getName().contains(".PNG")
                            ) {
                        String path = imagePath.getAbsolutePath();
                        GallerySinglePicture picture = new GallerySinglePicture(path, false);
                        resultIAV.add(picture);

                    }
                }
                //  }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return resultIAV;

    }

    public String getmCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

}

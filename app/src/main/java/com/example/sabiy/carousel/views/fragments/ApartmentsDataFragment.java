package com.example.sabiy.carousel.views.fragments;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.database.DatabaseIO;
import com.example.sabiy.carousel.database.FavoritePropositions;
import com.example.sabiy.carousel.viewModels.ApartmentDataFragmentViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.List;
import java.util.Objects;

public class ApartmentsDataFragment extends BaseFragment {

    private MapView mapView;

    private ViewPager viewPager;
    private TextView price;
    private TextView label;
    private TextView desc;
    private TextView type;
    private TextView operationType;
    private  TextView houseType;
    private TextView area;
    private TextView roomsCount;
    private TextView floorsCount;
    private TextView floor;
    private TextView reviews;
    private ShineButton like;
    private ApartmentDataFragmentViewModel viewModel;

    private boolean checkIfAlreadyHavePermission() {
        int result = ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.GET_ACCOUNTS);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(ApartmentDataFragmentViewModel.class);
        viewModel.setHouse(getArguments().getParcelable("house"));
        viewModel.onLatLngReady = (googleMap, latLng) -> {
            try {
                Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
                    googleMap.addMarker(new MarkerOptions().position(latLng).title(viewModel.getHouse().getAdress()));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

                });
            } catch (NullPointerException e) {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(() -> {

                        googleMap.addMarker(new MarkerOptions().position(latLng).title(viewModel.getHouse().getAdress()));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

                    });
                }
            }
        };
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.CALL_PHONE}, 101);
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        CoordinatorLayout view = (CoordinatorLayout) inflater.inflate(R.layout.apartment_fragment, container, false);
        viewPager = view.findViewById(R.id.pictures);

        LinearLayout contacts = view.findViewById(R.id.contacts);
        FloatingActionButton callButton = view.findViewById(R.id.call_button);
        TelephonyManager telephonyManager = (TelephonyManager) Objects.requireNonNull(getActivity()).getSystemService(Context.TELEPHONY_SERVICE);

        if (Objects.requireNonNull(telephonyManager).getSimState() == TelephonyManager.SIM_STATE_ABSENT)
            callButton.hide();

        callButton.setOnClickListener(v -> {
            int MyVersion = Build.VERSION.SDK_INT;
            if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (!checkIfAlreadyHavePermission()) {
                    requestForSpecificPermission();
                }
            }
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + viewModel.getHouse().getPhoneNumber1().replace("+380", "").replace("-", "")));
            startActivity(intent);
        });

        like = view.findViewById(R.id.favourite);
        AppBarLayout collapsingToolbarLayout = view.findViewById(R.id.appBarLayout2);
        collapsingToolbarLayout.addOnOffsetChangedListener((appBarLayout, i) -> {
            if (Math.abs(i) - appBarLayout.getTotalScrollRange() == 0) {
                like.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.button_apear_animation));
                like.setElevation(getResources().getDimension(R.dimen.like_elevation));
                like.setBackgroundResource(R.drawable.collapsed_bar_like_background);
            } else if (i == 0) {
                like.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.button_apear_animation));
                like.setElevation(0);
                like.setElevation(getResources().getDimension(R.dimen.like_elevation));
                like.setBackgroundResource(android.R.color.transparent);
            }
        });


        like.setOnClickListener(v -> {
            FavoritePropositions propositions = new FavoritePropositions();
            propositions.setId(viewModel.getHouse().getId());

            if (!like.isChecked()) {
                DatabaseIO.delete(viewModel.getHouse().getId());
            } else {
                DatabaseIO.insert(viewModel.getHouse().getId());
            }
        });


        if (contacts != null)
        contacts.setOnClickListener(v -> {
            SellerDataDialogFragment dialogFragment = new SellerDataDialogFragment();
            dialogFragment.show(getActivity().getSupportFragmentManager(), "");
        });

        ImageButton backButton = view.findViewById(R.id.back_button);
        backButton.setOnClickListener(v -> getActivity().onBackPressed());

        price = view.findViewById(R.id.price);
        label = view.findViewById(R.id.label);
        desc = view.findViewById(R.id.description);
        type = view.findViewById(R.id.type);
        operationType = view.findViewById(R.id.operation_type);
        houseType = view.findViewById(R.id.house_type);
        area = view.findViewById(R.id.area);
        roomsCount = view.findViewById(R.id.rooms_count);
        floorsCount = view.findViewById(R.id.floors_count);
        floor = view.findViewById(R.id.floor);
        reviews = view.findViewById(R.id.reviews);

        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onSaveInstanceState(savedInstanceState);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


    @Override
    public void onResume() {

        DatabaseIO.isEmpty(viewModel.getHouse().getId(), (List<Integer> ids) -> {
            if (!ids.isEmpty()) {
                like.setChecked(true);
            }
        });

        super.onResume();
        mapView.onResume();
        price.setText(viewModel.getHouse().getPrice() + " $");
        label.setText(viewModel.getHouse().getLabel());
        desc.setText(viewModel.getHouse().getDescription());
        type.setText(viewModel.getHouse().getSaleType());
        operationType.setText(viewModel.getHouse().getOperationType());
        houseType.setText(viewModel.getHouse().getBuildingType());
        area.setText(viewModel.getHouse().getArea().toString());
        roomsCount.setText(viewModel.getHouse().getRoomsCount() + "");
        floorsCount.setText(viewModel.getHouse().getFloorsCount() + "");
        floor.setText(viewModel.getHouse().getApartmentsFloor() + "");
        reviews.setText(viewModel.getHouse().getReviews());
        viewPager.setAdapter(viewModel.initAdapter());
        viewPager.setOffscreenPageLimit(viewModel.getHouse().getPictures().size());
        mapView.getMapAsync(viewModel.onMapReadyCallback);
    }
}

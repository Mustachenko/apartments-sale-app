package com.example.sabiy.carousel.adapters;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sabiy.carousel.API.House;
import com.example.sabiy.carousel.views.fragments.ApartmentsDataFragment;

import java.util.List;

public class ApartmentDataAdapter extends FragmentStatePagerAdapter {
    private List<House> houses;
    private OnLastInList onLastInList;


    public ApartmentDataAdapter(FragmentManager fm, List<House> houses) {
        super(fm);
        this.houses = houses;
        notifyDataSetChanged();
    }

    public void setOnLastInList(OnLastInList onLastInList) {
        this.onLastInList = onLastInList;
    }

    public interface OnLastInList {
        void onLast();
    }

    public void updateHouses(List<House> newHouses) {
        houses.addAll(newHouses);
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return houses.size();
    }

    @Override
    public ApartmentsDataFragment getItem(int i) {
      //  if(i == houses.size() -1) {
       //     onLastInList.onLast();
        //}
        ApartmentsDataFragment fragment = new ApartmentsDataFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("house", houses.get(i));
        fragment.setArguments(bundle);
        return fragment;
    }

}

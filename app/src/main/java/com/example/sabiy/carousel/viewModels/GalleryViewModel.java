package com.example.sabiy.carousel.viewModels;

import android.arch.lifecycle.ViewModel;

import com.example.sabiy.carousel.adapters.PicturesPagerAdapter;
import com.example.sabiy.carousel.adapters.SelectablePicturesPagerAdapter;
import com.example.sabiy.carousel.dataWrapper.GallerySinglePicture;

import java.util.ArrayList;

public class GalleryViewModel extends ViewModel {
    private ArrayList<GallerySinglePicture> checkablePictures;
    private ArrayList<String> pictures;
    private SelectablePicturesPagerAdapter selectablePicturesPagerAdapter;
    private PicturesPagerAdapter picturesPagerAdapter;

    public void setCheckablePictures(ArrayList<GallerySinglePicture> checkablePictures) {
        this.checkablePictures = checkablePictures;
    }

    public ArrayList<String> getPictures() {
        return pictures;
    }

    public void setPictures(ArrayList<String> pictures) {
        this.pictures = pictures;
    }

    public PicturesPagerAdapter initPicturesPagerAdapter() {
        if (picturesPagerAdapter == null)
            picturesPagerAdapter = new PicturesPagerAdapter(pictures, true);
        return picturesPagerAdapter;
    }

    public SelectablePicturesPagerAdapter initSelectablePicturesPagerAdapter() {
        if (selectablePicturesPagerAdapter == null)
            selectablePicturesPagerAdapter = new SelectablePicturesPagerAdapter(checkablePictures);
        return selectablePicturesPagerAdapter;
    }

    public SelectablePicturesPagerAdapter getSelectablePicturesPagerAdapter() {
        return selectablePicturesPagerAdapter;
    }
}

package com.example.sabiy.carousel.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.example.sabiy.carousel.R;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class SellerDataDialogFragment extends BottomSheetDialogFragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Objects.requireNonNull(getDialog().getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        ConstraintLayout root = (ConstraintLayout) inflater.inflate(R.layout.fragment_cabinet_passport, container, false);
        CircleImageView userPhoto = root.findViewById(R.id.circleImageView);
        userPhoto.bringToFront();
        root.removeView(root.findViewById(R.id.edit_user));
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        FrameLayout bottomSheet = getDialog().findViewById(android.support.design.R.id.design_bottom_sheet);

        BottomSheetBehavior.from(bottomSheet)
                .setState(BottomSheetBehavior.STATE_EXPANDED);


    }
}

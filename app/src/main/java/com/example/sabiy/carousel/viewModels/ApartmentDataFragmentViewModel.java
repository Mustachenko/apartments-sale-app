package com.example.sabiy.carousel.viewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.example.sabiy.carousel.API.GeocodingLocation;
import com.example.sabiy.carousel.API.House;
import com.example.sabiy.carousel.adapters.PicturesPagerAdapter;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;


public class ApartmentDataFragmentViewModel extends AndroidViewModel {
    public OnLatLngReady onLatLngReady;
    private House house;
    private LatLng latLng;
    public OnMapReadyCallback onMapReadyCallback = googleMap -> {

        Thread thread = new Thread(() -> {
            latLng = GeocodingLocation.getAddressFromLocation(house.getAdress(),
                    getApplication());
            if (latLng != null) {
                onLatLngReady.onReady(googleMap, latLng);
            }
        });
        thread.start();
    };
    private PicturesPagerAdapter adapter;

    public ApartmentDataFragmentViewModel(@NonNull Application application) {
        super(application);
    }

    public PicturesPagerAdapter getAdapter() {
        return adapter;
    }

    public PicturesPagerAdapter initAdapter() {
        if (adapter == null)
            adapter = new PicturesPagerAdapter((ArrayList<String>) house.getPictures(), false);
        return adapter;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public interface OnLatLngReady {
        void onReady(GoogleMap googleMap, LatLng latLng);
    }


}

package com.example.sabiy.carousel.API;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.sabiy.carousel.dataWrapper.HouseParameters;
import com.example.sabiy.carousel.dataWrapper.Seller;
import com.example.sabiy.carousel.dataWrapper.SellersHouseDescription;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class House implements Parcelable {

    public static final Creator<House> CREATOR = new Creator<House>() {
        @Override
        public House createFromParcel(Parcel in) {
            return new House(in);
        }

        @Override
        public House[] newArray(int size) {
            return new House[size];
        }
    };

    protected House(Parcel in) {
        id = in.readInt();
        reviews = in.readInt();
        description = in.readParcelable(SellersHouseDescription.class.getClassLoader());
        seller = in.readParcelable(Seller.class.getClassLoader());
        parameters = in.readParcelable(HouseParameters.class.getClassLoader());
    }

    public String getReviews() {
        return "переглядів: " + reviews;
    }

    public void setReviews(Integer reviews) {
        this.reviews = reviews;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(id);
        dest.writeInt(reviews);
        dest.writeParcelable(description, flags);
        dest.writeParcelable(seller, flags);
        dest.writeParcelable(parameters, flags);

    }







    @SerializedName("id")
    private Integer id;

    @SerializedName("reviews")
    private Integer reviews;

    @SerializedName("main")
    private SellersHouseDescription description;

    @SerializedName("parameters")
    private HouseParameters parameters;

   @SerializedName("seller")
    private Seller seller;

    public String getPreview(){return description.preview;}
    public String getLabel(){return description.label;}
    public String getDescription(){return description.desc;}
    public boolean favourite() {return false;}
    public Double getPrice() {return description.prise;}
    public String getAdress() {return parameters.address;}
    public String getSaleType() { return parameters.sale_type;}
    public String getOperationType() { return parameters.operation_type;}
    public String getBuildingType() { return parameters.building_type;}
    public Double getArea() { return parameters.area;}
    public int getRoomsCount() {return parameters.rooms;}
    public int getFloorsCount() { return parameters.floors;}
    public int getApartmentsFloor() { return parameters.floor;}
    public List<String> getPictures() { return description.pictures;}
    public String getSellerName() {return seller.name;}
    public String getPhoneNumber1() { return seller.phone_number1;}
    public String getPhoneNumber2() { return seller.phone_number2;}
    public String getEmail() {return seller.email;}
    public Integer getId() { return id; }
}

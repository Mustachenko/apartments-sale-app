package com.example.sabiy.carousel.views.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.dataWrapper.GallerySinglePicture;
import com.example.sabiy.carousel.viewModels.GridGalleryViewModel;
import com.example.sabiy.carousel.views.fragments.NewPostFragment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class GridGalleryActivity extends AppCompatActivity {
    private static final int CAMERA_REQUEST = 231;

    private GridGalleryViewModel viewModel;
    public static final String MODE = "mode";
    public static final String PICTURES = "pictures";


    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(GridGalleryViewModel.class);
        setContentView(R.layout.activity_grid_gallery);


        RecyclerView recyclerView = findViewById(R.id.pictures);
        int span = (int) getResources().getDimension(R.dimen.gallerySpan);
        recyclerView.setLayoutManager(new GridLayoutManager(this, span));

        //if previous activity already contains this pictures then make them checked
        ArrayList<String> checkedItems = getIntent().getStringArrayListExtra(PICTURES);
        recyclerView.setAdapter(viewModel.initAdapter(getIntent().getBooleanExtra(MODE, false), checkedItems));

        if (!viewModel.getAdapter().isOnePictureMode()) {
            FloatingActionButton done = findViewById(R.id.done);
            done.setVisibility(View.VISIBLE);
            done.setOnClickListener((v) -> end());
        }

        viewModel.getAdapter().setUserPicture = this::end;

        viewModel.getAdapter().openFullScreenGallery = (int i) -> {
            Intent intent = new Intent(this, GalleryActivity.class);
            intent.putExtra(GalleryActivity.SELECTABLE, true);
            intent.putExtra("position", i);
            intent.putParcelableArrayListExtra(PICTURES, (ArrayList<? extends Parcelable>) viewModel.getAdapter().getPictures());
            startActivityForResult(intent, GalleryActivity.REQUEST_CODE);
        };

        viewModel.getAdapter().onCameraClickListener = () -> {
                if (checkSelfPermission(Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            CAMERA_REQUEST);
                } else {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File file = null;
                try {
                    file = viewModel.createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(file != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            "com.example.sabiy.fileprovider",
                            file);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    GridGalleryActivity.this.startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GalleryActivity.REQUEST_CODE) {
            ArrayList<GallerySinglePicture> pictures = data != null ? data.getParcelableArrayListExtra(PICTURES) : null;
            viewModel.getAdapter().updateItems(pictures);
        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Intent resultIntent = new Intent();
            if (viewModel.getAdapter().isOnePictureMode()) {
                resultIntent.putExtra("data", viewModel.getmCurrentPhotoPath());
                setResult(EditCabinetPassportActivity.USER_PHOTO_REQUEST_CODE, resultIntent);

            } else {
                ArrayList<String> exportPhotos = new ArrayList<>();
                exportPhotos.add(viewModel.getmCurrentPhotoPath());
                resultIntent.putStringArrayListExtra("data", exportPhotos);
                setResult(NewPostFragment.NEW_PHOTO_RESULT_CODE, resultIntent);
            }
            finish();
        } else if (viewModel.getmCurrentPhotoPath() != null) {
            new File(viewModel.getmCurrentPhotoPath()).delete();
        }

    }

    private void end() {
        Intent resultIntent = new Intent();
        ArrayList<String> exportPictures = (ArrayList<String>) viewModel.getAdapter().getExportPictures();
        if(!exportPictures.isEmpty()) {
            if (viewModel.getAdapter().isOnePictureMode()) {
                resultIntent.putExtra("data", exportPictures.get(0));
                setResult(EditCabinetPassportActivity.USER_PHOTO_REQUEST_CODE, resultIntent);

            } else {
                resultIntent.putStringArrayListExtra("data", exportPictures);
                setResult(NewPostFragment.NEW_PHOTO_RESULT_CODE, resultIntent);
            }
            finish();
        }
    }

}

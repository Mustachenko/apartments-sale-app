package com.example.sabiy.carousel.callbacks;

import android.support.v7.util.DiffUtil;

import com.example.sabiy.carousel.dataWrapper.GallerySinglePicture;

import java.util.List;

public class PicturesDiffUtillCallback extends DiffUtil.Callback {
    private List<GallerySinglePicture> oldPictures;
    private List<GallerySinglePicture> newPictures;

    public PicturesDiffUtillCallback(List<GallerySinglePicture> oldPictures, List<GallerySinglePicture> newPictures) {
        this.oldPictures = oldPictures;
        this.newPictures = newPictures;
    }

    @Override
    public int getOldListSize() {
        return oldPictures.size();
    }

    @Override
    public int getNewListSize() {
        return newPictures.size();
    }

    @Override
    public boolean areItemsTheSame(int i, int i1) {
        return oldPictures.get(i).getPicture().equals(newPictures.get(i1).getPicture());
    }

    @Override
    public boolean areContentsTheSame(int i, int i1) {
        return oldPictures.get(i).getPicture().equals(newPictures.get(i1).getPicture()) && oldPictures.get(i).isActive() == newPictures.get(i1).isActive();
    }
}

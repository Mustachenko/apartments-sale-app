package com.example.sabiy.carousel.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;


@Dao
public interface PropositionsDAO {
    @Query("SELECT * FROM FavoritePropositions")
    Flowable<List<Integer>> getAllFavoritePropositions();

    @Insert
    void insertFavorite(FavoritePropositions favoritePropositions);

    @Delete
    void deleteFavorite(FavoritePropositions favoritePropositions);

    @Update
    void updateFavorite(FavoritePropositions favoritePropositions);

    @Query("SELECT * FROM FavoritePropositions WHERE id IS :id")
    Single<List<Integer>> isFavorite(int id);
}

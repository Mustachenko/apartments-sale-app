package com.example.sabiy.carousel.views.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.sabiy.carousel.ArartmentsPageTransformer;
import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.adapters.PropositionsAdapter;
import com.example.sabiy.carousel.viewModels.PropositionsViewModel;

public class PropositionsActivity extends AppCompatActivity {
    private ViewPager viewPager;


    private int startPosition;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PropositionsAdapter.POSITION, startPosition);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PropositionsViewModel viewModel = ViewModelProviders.of(this).get(PropositionsViewModel.class);
        viewPager = findViewById(R.id.viewpager);
            Integer savedPosition = savedInstanceState != null ? savedInstanceState.getInt(PropositionsAdapter.POSITION) : 0;
            if(savedPosition != 0) {
                startPosition = savedPosition;
            } else {
                startPosition = getIntent().getExtras().getInt(PropositionsAdapter.POSITION);
            }

        viewModel.setHouses(getIntent().getParcelableArrayListExtra(PropositionsAdapter.HOUSES));
        viewPager.setAdapter(viewModel.initAdapter(getSupportFragmentManager()));
        viewPager.setOffscreenPageLimit(1);
        viewPager.setCurrentItem(startPosition);
        viewPager.setPageTransformer(true, new ArartmentsPageTransformer());

    }

}

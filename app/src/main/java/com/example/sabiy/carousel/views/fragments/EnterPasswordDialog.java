package com.example.sabiy.carousel.views.fragments;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.sabiy.carousel.R;

import java.util.Objects;

public class EnterPasswordDialog extends DialogFragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Objects.requireNonNull(getDialog().getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
        ConstraintLayout root = (ConstraintLayout) inflater.inflate(R.layout.text_input_field, container, false);
        Button cancel = root.findViewById(R.id.cancel);
        Button ok = root.findViewById(R.id.ok);
        cancel.setOnClickListener(v -> getDialog().cancel());
        EditText oldPassword = root.findViewById(R.id.old_pass);
        TextInputLayout currentPasswordInputField = root.findViewById(R.id.current_password);
        TextInputLayout passwordInputField = root.findViewById(R.id.new_password);
        EditText newPassword = root.findViewById(R.id.new_pass);
        ok.setOnClickListener(v -> {
            boolean isPassOk  = true;
            if (oldPassword.getText().toString().isEmpty()){
                TransitionDrawable transition = (TransitionDrawable) currentPasswordInputField.getBackground();
                errorAnimation(transition);
                isPassOk = false;
            }
            if (newPassword.getText().toString().isEmpty()) {
                TransitionDrawable transition = (TransitionDrawable) passwordInputField.getBackground();
                errorAnimation(transition);
                isPassOk = false;
            }
        });

        return root;
    }

    private void errorAnimation(TransitionDrawable transition) {
        transition.startTransition(400);
        new Handler().postDelayed(() -> transition.reverseTransition(400), 400);
    }
}

package com.example.sabiy.carousel.API;

public class PostHouse {


    private int user_id;
    private String address;
    private String desc;
    private int price;
    private String contact;
    private int tel1;
    private int tel2;
    private int floors;

    private String email;
    private int active;
    // private String house_type;
    //private String operation_type;
    private int area;
    private int rooms;
    private int floor;
    private int holder;

    public PostHouse(int user_id, String address, String desc, int price, String contact, int tel1, int tel2, String email, Integer active, int area, int rooms, int floor, int floors, int holder) {
        this.contact = contact;
        this.user_id = user_id;
        this.price = price;
        this.tel1 = tel1;
        this.tel2 = tel2;
        this.desc = desc;
        this.address = address;
        this.email = email;
        this.active = active;

        this.area = area;
        this.rooms = rooms;

        this.floor = floor;
        this.floors = floors;
        this.holder = holder;
    }

    public String getContact() {
        return contact;
    }


    public int getUser_id() {
        return user_id;
    }

    public int getPrice() {
        return price;
    }

    public int getTel1() {
        return tel1;
    }

    public int getTel2() {
        return tel2;
    }

    public String getDesc() {
        return desc;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public Integer getActive() {
        return active;
    }

    //  public String getHouse_type() {
    //      return house_type;
    //   }

    //  public String getOperation_type() {
    //    return operation_type;
    //  }

    public Integer getArea() {
        return area;
    }

    public Integer getRooms() {
        return rooms;
    }

    public Integer getFloor() {
        return floor;
    }

    public Integer getHolder() {
        return holder;
    }
}

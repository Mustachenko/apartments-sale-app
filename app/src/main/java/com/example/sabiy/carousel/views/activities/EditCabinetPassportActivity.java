package com.example.sabiy.carousel.views.activities;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.viewModels.EditCabinetPassportViewModel;
import com.example.sabiy.carousel.views.fragments.EnterPasswordDialog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditCabinetPassportActivity extends AppCompatActivity {
    public static final int USER_PHOTO_REQUEST_CODE = 6312;
    private EditCabinetPassportViewModel viewModel;
    private CircleImageView userPhoto;
    private int size;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == USER_PHOTO_REQUEST_CODE ) {
            viewModel.userImagePath = data != null ? data.getStringExtra("data") : null;
            if (viewModel.userImagePath != null)
                userPhoto.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
            if (viewModel.userImagePath != null) {

                Picasso.get().load(new File(viewModel.userImagePath)).centerCrop().resize(size, size).into(userPhoto);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        size = getResources().getDimensionPixelSize(R.dimen.user_image_size);
        viewModel = ViewModelProviders.of(this).get(EditCabinetPassportViewModel.class);
        setContentView(R.layout.activity_edit_cabinet_passport);
        userPhoto = findViewById(R.id.circleImageView);
        if (viewModel.userImagePath != null) {
            Picasso.get().load(new File(viewModel.userImagePath)).centerCrop().resize(size, size).into(userPhoto);
        }

        TextView password = findViewById(R.id.password);

        password.setOnClickListener(v -> {
            EnterPasswordDialog dialog = new EnterPasswordDialog();
            dialog.show(getSupportFragmentManager(), "Password");
        });

        ImageView changeUserPhotoImage = findViewById(R.id.changeImage);
        userPhoto.bringToFront();
        changeUserPhotoImage.bringToFront();
        changeUserPhotoImage.setOnClickListener(v -> {
            if (checkIfAlreadyHaveCameraPermission()) {
                Intent intent = new Intent(this, GridGalleryActivity.class);
                intent.putExtra(GridGalleryActivity.MODE, true);
                startActivityForResult(intent, USER_PHOTO_REQUEST_CODE);
            } else requestPermissions(new String[]{Manifest.permission.CAMERA},
                    523);
        });
    }

    public boolean checkIfAlreadyHaveCameraPermission() {
        int result = ContextCompat.checkSelfPermission(Objects.requireNonNull(this), Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

}

package com.example.sabiy.carousel.dataWrapper;

import android.os.Parcel;
import android.os.Parcelable;

public class GallerySinglePicture implements Parcelable {
    private String picture;
    private boolean active;

    public static final Creator<GallerySinglePicture> CREATOR = new Creator<GallerySinglePicture>() {
        @Override
        public GallerySinglePicture createFromParcel(Parcel in) {
            return new GallerySinglePicture(in);
        }

        @Override
        public GallerySinglePicture[] newArray(int size) {
            return new GallerySinglePicture[size];
        }
    };

    public GallerySinglePicture(String picture, boolean active) {
        this.picture = picture;
        this.active = active;
    }

    public GallerySinglePicture(Parcel in) {
        picture = in.readString();
        active = in.readByte() != 0;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(picture);
        dest.writeByte((byte) (active ? 1 : 0));
    }
}

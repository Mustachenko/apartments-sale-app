package com.example.sabiy.carousel.dataWrapper;


public class Filtres{
    public static String sale_type;
    public static String operation_type;
    public static String building_type;
    public static double price_start;
    public static double price_end;
    public static double area_start;
    public static double area_end;
    public static int rooms;
    public static int floors;
    public static int floor_start;
    public static int floor_end;
    public static int start_from;
}
package com.example.sabiy.carousel.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.adapters.UserDataPagerAdapter;

public class CabinetFragment extends BaseFragment {


    private UserDataPagerAdapter mSectionsPagerAdapter;
    private LinearLayout root;
    public static final int CABINET_PASSPORT = 0;
    private ViewPager mViewPager;
    public static final int CABINET_POSTS = 1;
    private int currentItem;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            currentItem = savedInstanceState.getInt("fragment");
            System.out.println("onCreate " + currentItem);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mViewPager.getCurrentItem() == 0) {
            outState.putInt("fragment", CABINET_PASSPORT);
        } else if (mViewPager.getCurrentItem() == 1) {
            outState.putInt("fragment", CABINET_POSTS);
        }
        //Save the fragment's state here
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        root = (LinearLayout) inflater.inflate(R.layout.activity_cabinet, container, false);

        Toolbar toolbar = root.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSectionsPagerAdapter = new UserDataPagerAdapter(getChildFragmentManager());

        mViewPager = root.findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        TabLayout tabLayout = root.findViewById(R.id.tabs);

        if (currentItem == 1)
            mViewPager.setCurrentItem(CABINET_POSTS);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        return root;
    }
}

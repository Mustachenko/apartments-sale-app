package com.example.sabiy.carousel.adapters;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.views.activities.GridGalleryActivity;
import com.example.sabiy.carousel.views.fragments.NewPostFragment;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.refactor.library.SmoothCheckBox;

import static android.support.constraint.Constraints.TAG;
import static android.view.View.VISIBLE;

public class FromCameraPicturesAdapter extends RecyclerView.Adapter<FromCameraPicturesAdapter.ViewHolder> {
    private View root;
    private String previewImagePath = "";
    private ArrayList<String> pictures;
    private FloatingActionButton delete;
    private FloatingActionButton cancel;
    public List<String> getPictures() {
        return pictures;
    }

    public FromCameraPicturesAdapter(View root, ArrayList<String> pictures) {
        this.root = root;
        if (pictures.isEmpty() || !pictures.get(0).isEmpty())
            pictures.add(0, "");
        this.pictures = pictures;
    }

    private List<String> toRemovePictures = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RelativeLayout view = (RelativeLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.from_camera_picture_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    public String getPreview() {
        return previewImagePath;
    }

    public void addNewPicture(ArrayList<String> paths) {
        for (String s : paths) {
            if (!pictures.contains(s)) pictures.add(s);
            notifyItemInserted(pictures.size());
        }
    }

    private boolean checkIfAlreadyHavePermission(View view) {
        int result = ContextCompat.checkSelfPermission(Objects.requireNonNull(view.getContext()), Manifest.permission.READ_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.imageView = viewHolder.view.findViewById(R.id.picture);
        viewHolder.toDelete = viewHolder.view.findViewById(R.id.check_to_delete);

        if (pictures.get(i).equals(pictures.get(0))) {
            viewHolder.imageView.setImageResource(R.drawable.picture_library_center_inside);
            viewHolder.toDelete.setVisibility(View.GONE);
            viewHolder.view.setBackgroundColor(viewHolder.view.getContext().getResources().getColor(android.R.color.transparent));
            viewHolder.view.setOnClickListener(v -> {
                if (checkIfAlreadyHavePermission(v)) {
                    Intent intent = new Intent(viewHolder.imageView.getContext(), GridGalleryActivity.class);

                    ArrayList<String> presentPictures = new ArrayList<>(pictures);
                    presentPictures.remove(0);
                    intent.putStringArrayListExtra(GridGalleryActivity.PICTURES, presentPictures);
                    List<Fragment> fragments = ((AppCompatActivity) v.getContext()).getSupportFragmentManager().getFragments();
                    if (fragments.get(fragments.size() - 1) instanceof NewPostFragment)
                        fragments.get(fragments.size() - 1).startActivityForResult(intent, NewPostFragment.NEW_PHOTO_RESULT_CODE);
                }
            });
        } else {

            int pictureSize = viewHolder.imageView.getContext().getResources().getDimensionPixelOffset(R.dimen.cameraPictureSize);
            Picasso.get().load(new File(pictures.get(i)).toURI().toString()).centerCrop().resize(pictureSize, pictureSize).into(viewHolder.imageView);
            ImageView preview = root.findViewById(R.id.preview);
            viewHolder.view.setOnClickListener(v -> {
                try {
                    previewImagePath = pictures.get(viewHolder.getAdapterPosition());
                    preview.startAnimation(AnimationUtils.loadAnimation(viewHolder.view.getContext(), R.anim.fade_in));
                    Picasso.get().load(new File(pictures.get(i))).centerCrop().resize(pictureSize, pictureSize).into(preview);
                    viewHolder.imageView.setActivated(true);
                } catch (IndexOutOfBoundsException e) {
                    Log.e(TAG, "onBindViewHolder: ", e);
                }

                viewHolder.view.startAnimation(AnimationUtils.loadAnimation(viewHolder.view.getContext(), R.anim.click_animation));

            });
            delete = root.findViewById(R.id.delete);
            cancel = root.findViewById(R.id.cancel);
            Log.i(TAG, "onBindViewHolder: " + delete.getVisibility());

            if (delete.getVisibility() != View.GONE) {
                startDeleteMode(viewHolder);
            } else exitDeleteMode(viewHolder);

            if (toRemovePictures.contains(pictures.get(i))) {
                viewHolder.toDelete.setChecked(true);
            } else {
                viewHolder.toDelete.setChecked(false);
            }

            viewHolder.view.setOnLongClickListener(v -> {
                v.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.long_click_animation));
                startDeleteMode(viewHolder);
                return true;
            });

            cancel.setOnClickListener(v -> exitDeleteMode(viewHolder));

            delete.setOnClickListener(v -> {
                pictures.removeAll(toRemovePictures);
                if (toRemovePictures.contains(previewImagePath)) {
                    preview.startAnimation(AnimationUtils.loadAnimation(viewHolder.view.getContext(), R.anim.fade_out));
                    preview.setImageResource(R.drawable.ic_baseline_image_24px);
                    preview.startAnimation(AnimationUtils.loadAnimation(viewHolder.view.getContext(), R.anim.fade_in));
                    previewImagePath = "";
                }
                exitDeleteMode(viewHolder);
            });


            viewHolder.toDelete.setOnClickListener((v) -> {
                int position = viewHolder.getAdapterPosition();
                if (!pictures.get(position).isEmpty() && !toRemovePictures.contains(pictures.get(position))) {
                    toRemovePictures.add(pictures.get(position));
                    viewHolder.toDelete.setChecked(true, true);
                } else if (!toRemovePictures.isEmpty()) {
                    toRemovePictures.remove(pictures.get(viewHolder.getAdapterPosition()));
                    viewHolder.toDelete.setChecked(false, true);
                }
            });
        }
    }

    @SuppressLint("RestrictedApi")
    private void exitDeleteMode(ViewHolder viewHolder) {
        //if item is in deleteMode, then exit
        if (delete.getVisibility() == View.VISIBLE) {
            delete.startAnimation(AnimationUtils.loadAnimation(delete.getContext(), R.anim.button_gone_animation));
            cancel.startAnimation(AnimationUtils.loadAnimation(delete.getContext(), R.anim.button_gone_animation));
            toRemovePictures.clear();
            new Handler().postDelayed(() ->   {
                notifyDataSetChanged();
                delete.setVisibility(View.GONE);
                cancel.setVisibility(View.GONE);
            }, 200);
        }
        //only after gone animation end on first change
        if (delete.getVisibility() == View.GONE) {
            viewHolder.toDelete.setChecked(false);
            viewHolder.toDelete.startAnimation(AnimationUtils.loadAnimation(delete.getContext(), R.anim.button_gone_animation));
            viewHolder.toDelete.postOnAnimation(() -> viewHolder.toDelete.setVisibility(View.GONE));
        }
    }

    @SuppressLint("RestrictedApi")
    private void startDeleteMode(ViewHolder viewHolder) {
        viewHolder.toDelete.setVisibility(View.VISIBLE);
        viewHolder.toDelete.bringToFront();
        viewHolder.toDelete.startAnimation(AnimationUtils.loadAnimation(viewHolder.imageView.getContext(), R.anim.button_apear_animation));
        if (delete.getVisibility() == View.GONE) {
            delete.setVisibility(View.VISIBLE);
            delete.startAnimation(AnimationUtils.loadAnimation(root.getContext(), R.anim.button_apear_animation));
            cancel.setVisibility(View.VISIBLE);
            cancel.startAnimation(AnimationUtils.loadAnimation(root.getContext(), R.anim.button_apear_animation));
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout view;
        ImageView imageView;
        SmoothCheckBox toDelete;

        ViewHolder(@NonNull RelativeLayout itemView) {
            super(itemView);
            view = itemView;
        }
    }
}
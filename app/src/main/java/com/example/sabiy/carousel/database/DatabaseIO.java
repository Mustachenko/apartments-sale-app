package com.example.sabiy.carousel.database;

import android.util.Log;

import com.example.sabiy.carousel.App;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static android.support.constraint.Constraints.TAG;

public class DatabaseIO {

    private static PropositionsDAO dao = App.getInstance().getDatabase().propositionsDAO();

    public static void insert(int id) {

        Completable.fromAction(() -> {
            FavoritePropositions propositions = new FavoritePropositions();
            propositions.setId(id);

            dao.insertFavorite(propositions);
        }).

                subscribeOn(Schedulers.io()).

                observeOn(AndroidSchedulers.mainThread()).

                subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.i(TAG, "new favorite proposition");
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    public static void delete(int id) {
        Completable.fromAction(() -> {
            FavoritePropositions propositions = new FavoritePropositions();
            propositions.setId(id);
            dao.deleteFavorite(propositions);
        }).subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.i(TAG, "proposition deleted");
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    public static void getAll() {
        dao.getAllFavoritePropositions().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

                .subscribe((List<Integer> ids) -> {
                    Log.i(TAG, "getAll: " + ids.toString());
                });
    }

    public static boolean isEmpty(int id, Consumer<List<Integer>> consumer) {
        boolean[] empty = new boolean[1];
        empty[0] = true;
        dao.isFavorite(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

                .subscribe(consumer);
        return empty[0];
    }
}

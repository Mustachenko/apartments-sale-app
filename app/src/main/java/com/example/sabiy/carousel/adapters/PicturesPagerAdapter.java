package com.example.sabiy.carousel.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.sabiy.carousel.R;
import com.example.sabiy.carousel.views.activities.GalleryActivity;
import com.ortiz.touch.TouchImageView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

public class PicturesPagerAdapter extends PagerAdapter {


    private LayoutInflater layoutInflater;
    private ArrayList<String> pictures;
    public boolean fullScreen;

    public void setFullScreen(boolean fullScreen) {
        this.fullScreen = fullScreen;
        notifyDataSetChanged();
    }

    public PicturesPagerAdapter( ArrayList<String> pictures, boolean fullScreen) {
        this.fullScreen = fullScreen;
        this.pictures = pictures;
    }

    @Override
    public int getCount() {
        return pictures.size();
   }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;

        if(fullScreen){
            view = layoutInflater.inflate(R.layout.scale_picture_layout, container, false);
            TouchImageView imageView = view.findViewById(R.id.picture);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            Picasso.get().load(pictures.get(position)).into(imageView);
        } else {
            view = layoutInflater.inflate(R.layout.picture_layout, container, false);
            ImageView imageView = view.findViewById(R.id.picture);
            imageView.setOnClickListener(view1 -> {

                        Intent intent = new Intent(container.getContext(), GalleryActivity.class);
                        intent.putExtra("pictures", pictures);
                        intent.putExtra("position", position);
                        container.getContext().startActivity(intent);

                    });
            Picasso.get().load(pictures.get(position)).into(imageView);
        }

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);
        return view;
    }



    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }
}
